package fr.tuto.spring.springjpadao.abstrait;

import fr.tuto.spring.springcommon.exception.DAOException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public abstract class AbstractJPA<E,T> implements GenericEmDao<E,T> {

    private static Logger logger = LoggerFactory.getLogger(AbstractJPA.class.getName());

    @PersistenceContext
    @Qualifier(value = "entityManager")
    private EntityManager entityManager;

    private String entityName;

    private Class<E> persistentClass;

    public EntityManager getEntityManager(){
        if (entityManager == null){
            throw new IllegalStateException("[FORMATION]: EntityManager has not been set on DAO before usage");
        }
        return entityManager;
    }


    public E findByPK(T entityId) {
        return entityManager.find(persistentClass, entityId);
    }


    public E creerEntity(final E entity){
        if (getEntityManager().contains(entity)){
            logger.error("Impossible de créer une entitée déjà existante {}", entity);
            throw new DAOException("Impossible de créer une entitée déjà existante");
        }
        return persistAndFlush(entity);
    }

    public E persistAndFlush(E transientEntity){
        try {
            getEntityManager().persist(transientEntity);
        } catch (ConstraintViolationException e) {
        }
        return transientEntity;
    }

    public static String getEntityNameForClass(Class<?> entityClass){
        Entity entity = entityClass.getAnnotation(Entity.class);

        String entityName;
        // Entity with specified name
        if ("".equals(entity.name())){
            entityName = entityClass.getName();
            // Entity with default name (Class name)
        }else{
            entityName = entity.name();
        }
        return entityName;
    }

    protected Class<E> getPersistentClass(){
        return persistentClass;
    }

    public void setPersistentClass(Class<E> persistentClass){
        this.persistentClass = persistentClass;
    }

    public long countAllEntity() {
        final StringBuilder requete = new StringBuilder("select count(*) from ");
        requete.append(this.persistentClass.getSimpleName());
        TypedQuery<Long> query = getEntityManager().createQuery(requete.toString(), Long.class);
        return query.getSingleResult().longValue();
    }

}
