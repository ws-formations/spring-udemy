package fr.tuto.spring.springjpadao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class FormdaojpaApplication implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(FormdaojpaApplication.class);

	@Override
	public void run(String... args){
		log.info("---------- RUN JPA DAO -----------");
	}
}
