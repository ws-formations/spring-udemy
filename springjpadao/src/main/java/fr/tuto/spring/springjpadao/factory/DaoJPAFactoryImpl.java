package fr.tuto.spring.springjpadao.factory;

import fr.tuto.spring.springjpadao.jpa.FormationJpaDao;
import fr.tuto.spring.springjpadao.jpa.UserJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DaoJPAFactoryImpl implements DaoJPAFactory {

    @Autowired
    private FormationJpaDao formationJpaDao;

    @Autowired
    private UserJpaDao userJpaDao;

    @Override
    public FormationJpaDao getFormationJpaDao() {
        return formationJpaDao;
    }

    @Override
    public UserJpaDao getUserJpaDao() {
        return userJpaDao;
    }
}
