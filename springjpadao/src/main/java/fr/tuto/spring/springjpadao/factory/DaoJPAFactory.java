package fr.tuto.spring.springjpadao.factory;


import fr.tuto.spring.springjpadao.jpa.FormationJpaDao;
import fr.tuto.spring.springjpadao.jpa.UserJpaDao;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public interface DaoJPAFactory {
    FormationJpaDao getFormationJpaDao();

    UserJpaDao getUserJpaDao();
}
