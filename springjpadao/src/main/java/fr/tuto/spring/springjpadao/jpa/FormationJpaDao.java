package fr.tuto.spring.springjpadao.jpa;

import fr.tuto.spring.springdomaine.jpa.FormationJpa;

import java.util.List;

public interface FormationJpaDao{

	FormationJpa createFormationJpa(FormationJpa formation);

	void updateFormationJpa(FormationJpa formation);

	void deleteFormationJpa(FormationJpa formation);

	FormationJpa findFormationJpaParID(Long idForm);

	List<FormationJpa> findAllFormationJpa();

	long getCountFormJpa();
}
