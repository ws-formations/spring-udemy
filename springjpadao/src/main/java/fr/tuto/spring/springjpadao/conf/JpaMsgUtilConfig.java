package fr.tuto.spring.springjpadao.conf;

import java.util.ResourceBundle;

public final class JpaMsgUtilConfig{

    private static final ResourceBundle profilBundle = ResourceBundle.getBundle("profil-groupeutil");
    private static ResourceBundle resourceBundleDev = ResourceBundle.getBundle("groupe-utildev");
    private static ResourceBundle resourceBundleProd = ResourceBundle.getBundle("database-jpa.properties");
    private static JpaMsgUtilConfig INSTANCE = null;

    private JpaMsgUtilConfig() {
    }

    public static JpaMsgUtilConfig INSTANCE() {
        if (INSTANCE == null) {

            synchronized (JpaMsgUtilConfig.class) {
                if (INSTANCE == null) {
                    INSTANCE = new JpaMsgUtilConfig();
                }
            }
        }
        return INSTANCE;
    }

    public static ResourceBundle getBundleProfil(){
        if (INSTANCE().profilActive().equals("dev")){
            return resourceBundleDev;
        }else{
            return resourceBundleProd;
        }
    }

    public String getMsg(String msg){
        return getBundleProfil().getString(msg);
    }

    public String profilActive(){
        return profilBundle.getString("app.profiles.active");
    }

    public String csvReaderStudent(){
        return getBundleProfil().getString("export.student.csvtodto.file.reader");
    }

    public String csvReaderVlib(){
        return getBundleProfil().getString("export.csv.file.reader.vlib");
    }

    public String csvFolderWriterVlib() {
        return getBundleProfil().getString("export.csv.folder.writer.vlib");
    }


}
