package fr.tuto.spring.springjpadao.jpa;

import fr.tuto.spring.springcommon.exception.DAOException;
import fr.tuto.spring.springdomaine.jpa.UserJpa;
import fr.tuto.spring.springjpadao.abstrait.AbstractJPA;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import java.util.List;

@Service
@Transactional("jpaPlatformTransactionManager")
public class UserJpaDaoImpl extends AbstractJPA<UserJpa, Long> implements UserJpaDao{


	public UserJpaDaoImpl() {
		super.setPersistentClass(UserJpa.class);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public UserJpa createUserJpa(UserJpa userJpa) {
		return creerEntity(userJpa);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public void updateUserJpa(final UserJpa userJpa) {
		UserJpa oldEntity = getEntityManager().find(UserJpa.class, userJpa.getId());
		oldEntity.setEmail(userJpa.getEmail());
		getEntityManager().persist(oldEntity);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public void deleteUserJpa(final UserJpa userJpa) {
		UserJpa oldEntity = getEntityManager().find(UserJpa.class, userJpa.getId());
		getEntityManager().remove(oldEntity);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public UserJpa findUserJpaParID(Long idUser) throws DAOException{
		UserJpa userJpa = null;
		try {
			userJpa = findByPK( idUser);

		}catch (Exception ex){
			throw new DAOException(" DAO Exception Can't find UserJpa for ID " + idUser);
		}finally {
			if (userJpa == null) {
				throw new EntityNotFoundException("FINALLY Can't find UserJpa for ID "
						+ idUser);
			}
		}
		return userJpa;
	}
	@Override
	public UserJpa findUserJpaByID(Long idUser) {
		StringBuilder builder = new StringBuilder();
		UserJpa userJpa;
		Query query = null;
		try {

			builder.append(" SELECT ");
			builder.append(" U.ID, U.DRINK_PREFERENCE, U.EMAIL, U.FIRST_NAME, ");
			builder.append(" U.LAST_NAME, U.PHOTOS, HB.USER_JPA_ID, HB.HOBBIES ");
			builder.append(" from user_jpa U ");
			builder.append(" left outer join USER_JPA_HOBBIES HB ");
			builder.append(" on U.ID=HB.USER_JPA_ID ");
			builder.append(" where U.ID = :id ");

			query = getEntityManager().createNativeQuery(builder.toString(), UserJpa.class);
			query.setParameter("id", idUser);
			System.out.println(query.toString());
			userJpa = (UserJpa) query.getSingleResult();

		} catch (RuntimeException e) {
			throw new EntityNotFoundException("Can't find UserJpa for ID " + idUser +query.toString());
		}
		return userJpa;
	}


	@Override
	public List<UserJpa> findAllUsersJpa() {
		Query query = getEntityManager().createQuery("select f from UserJpa f", UserJpa.class);
		return query.getResultList();
	}

	@Override
	public long getCountUserJpa() {
		return countAllEntity();
	}
}
