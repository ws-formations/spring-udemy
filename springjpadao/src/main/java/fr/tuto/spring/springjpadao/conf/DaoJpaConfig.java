package fr.tuto.spring.springjpadao.conf;

import fr.tuto.spring.springtechnique.confds.DaoJPATechnicalProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @ImportResource({"classpath:dbjpa/schema_jpa.sql","classpath:dbjpa/data_jpa.sql"})
 */
@Configuration
@ComponentScan(basePackages = {"fr.tuto.spring.springjpadao", "fr.tuto.spring.springtechnique" })
@EnableTransactionManagement
public class DaoJpaConfig{

	private static final Logger log = LoggerFactory.getLogger(DaoJpaConfig.class);

	@Autowired
	private DaoJPATechnicalProperties daoJPATechnicalProperties;


	@Bean("jpaDataSource")
	@Profile("!test")
	public DataSource jpaDataSource() {
		log.info("URRL Module JPA DAO = "+ daoJPATechnicalProperties.getUrlmysql("JPA"));
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(daoJPATechnicalProperties.getDbdriver());
		dataSource.setUrl(daoJPATechnicalProperties.getUrlmysql("JPA"));
		dataSource.setUsername(daoJPATechnicalProperties.getUser());
		dataSource.setPassword(daoJPATechnicalProperties.getPassword());
		return dataSource;
	}

	@Bean(name = "jpaDataSource")
	@Profile("test")
	public DataSource jpaDataSourceH2() {
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).addScript("classpath:dbjpa/schema_jpa.sql")
				.addScript("classpath:dbjpa/data_jpa.sql").build();
	}


}
