package fr.tuto.spring.springjpadao.jpa;

import fr.tuto.spring.springcommon.exception.DAOException;
import fr.tuto.spring.springdomaine.jpa.UserJpa;

import java.util.List;

public interface UserJpaDao {

	UserJpa createUserJpa(UserJpa userJpa);

	void updateUserJpa(UserJpa userJpa);

	void deleteUserJpa(UserJpa formation);

	UserJpa findUserJpaParID(Long artistId) throws DAOException;

    UserJpa findUserJpaByID(Long idUser);

    List<UserJpa> findAllUsersJpa();

	long getCountUserJpa();
}
