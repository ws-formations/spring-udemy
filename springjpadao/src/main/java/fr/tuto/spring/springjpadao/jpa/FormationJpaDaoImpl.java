package fr.tuto.spring.springjpadao.jpa;

import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springjpadao.abstrait.AbstractJPA;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import java.util.List;

@Service
@Transactional("jpaPlatformTransactionManager")
public class FormationJpaDaoImpl extends AbstractJPA<FormationJpa, Long> implements FormationJpaDao{


	public FormationJpaDaoImpl() {
		super.setPersistentClass(FormationJpa.class);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public FormationJpa createFormationJpa(final FormationJpa formation) {
		return creerEntity(formation);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public void updateFormationJpa(final FormationJpa formation) {
		FormationJpa oldEntity = getEntityManager().find(FormationJpa.class, formation.getId());
		oldEntity.setTitre(formation.getTitre());
		oldEntity.setDescriptif(formation.getDescriptif());
		getEntityManager().persist(oldEntity);
	}

	@Override
	@Transactional("jpaPlatformTransactionManager")
	public void deleteFormationJpa(final FormationJpa formation) {
		FormationJpa oldEntity = getEntityManager().find(FormationJpa.class, formation.getId());
		getEntityManager().remove(oldEntity);
	}

	@Override
	public FormationJpa findFormationJpaParID(Long artistId) {
		FormationJpa formationJpa = getEntityManager().find(FormationJpa.class, artistId);
		if (formationJpa == null) {
			throw new EntityNotFoundException("Can't find FormationJpa for ID "
					+ artistId);
		}
		return formationJpa;
	}

	@Override
	public List<FormationJpa> findAllFormationJpa() {
		Query query = getEntityManager().createQuery("select f from FormationJpa f", FormationJpa.class);
		return query.getResultList();
	}

	@Override
	public long getCountFormJpa() {
		return countAllEntity();
	}

}
