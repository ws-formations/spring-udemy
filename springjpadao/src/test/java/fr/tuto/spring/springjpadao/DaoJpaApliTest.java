package fr.tuto.spring.springjpadao;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@EntityScan("fr.tuto.spring.springdomaine.jpa")
@ComponentScan(basePackages = "fr.tuto.spring.springjpadao")
@SpringBootConfiguration
public class DaoJpaApliTest {

}
