package fr.tuto.spring.springjpadao.jpa;

import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springjpadao.DaoJpaApliTest;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ContextConfiguration(classes = { DaoJpaApliTest.class})
public class FormJPADaoTest {

	@Autowired
	private FormationJpaDao formationJpaDao;

	@Test
	@Transactional
	@DisplayName("Reussir un Rollback des insertions")
	public void should1CreateRunWithoutError() {
		FormationJpa formation = new FormationJpa("formation 1", "descriptif1");
		FormationJpa formJpa = formationJpaDao.createFormationJpa(formation);
		assertThat(formJpa.getId()).isNotNull();
		assertThat(formationJpaDao.findFormationJpaParID(formJpa.getId())).isNotNull();

		assertThat(formationJpaDao.findFormationJpaParID(1l)).isEqualTo(formJpa);
	}

	@BeforeTransaction
	public void beforeTx() {
		assertThat(formationJpaDao.findAllFormationJpa()).hasSize(0);
	}

	@AfterTransaction
	public void afterTx() {
		assertThat(formationJpaDao.findAllFormationJpa()).hasSize(0);
	}

	@Test
	@DisplayName("Reussir une mise à jour en base")
	public void should2UpdateRunWithoutError() {
		FormationJpa formation = new FormationJpa("formation 2", "descriptif2");
		FormationJpa newForm = formationJpaDao.createFormationJpa(formation);

		newForm.setTitre("titre modifié");
		formationJpaDao.updateFormationJpa(newForm);

		assertThat(formationJpaDao.findFormationJpaParID(formation.getId()).getTitre()).isEqualTo("titre modifié");
	}

	@Test
	@DisplayName("Reussir le test de EntityNotFoundException")
	public void should3DeleteFormationJpa() {
		FormationJpa formation = new FormationJpa("formation 3", "descriptif3");
		FormationJpa form = formationJpaDao.createFormationJpa(formation);
		formationJpaDao.deleteFormationJpa(form);

		Assertions.assertThrows(EntityNotFoundException.class, () -> {
			assertThat(formationJpaDao.findFormationJpaParID(form.getId())).isNull();

		});

	}

	@Test
	@Sql(scripts = "classpath:dbjpa/data_jpa.sql")
	@DisplayName("Reussir une insertion des scripts sql")
	public void should4FindAllSqlScript() {
		assertThat(formationJpaDao.findAllFormationJpa()).hasSize(4);
	}

}
