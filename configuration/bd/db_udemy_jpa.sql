-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 30 Septembre 2020 à 21:29
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_udemy_jpa`
--

-- --------------------------------------------------------

--
-- Structure de la table `formation_jpa`
--

CREATE TABLE `formation_jpa` (
  `ID` bigint(20) NOT NULL,
  `DESC_FORM` varchar(255) NOT NULL,
  `TITRE_FORM` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `formation_jpa`
--

INSERT INTO `formation_jpa` (`ID`, `DESC_FORM`, `TITRE_FORM`) VALUES
(1, 'desc', 'titre'),
(2, 'desc', 'titre'),
(3, 'desc', 'titre'),
(4, 'desc', 'titre');

-- --------------------------------------------------------

--
-- Structure de la table `seq_formation`
--

CREATE TABLE `seq_formation` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `seq_formation`
--

INSERT INTO `seq_formation` (`next_val`) VALUES
(1),
(1);

-- --------------------------------------------------------

--
-- Structure de la table `user_jpa`
--

CREATE TABLE `user_jpa` (
  `ID` bigint(20) NOT NULL,
  `DRINK_PREFERENCE` varchar(255) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,
  `PHOTOS` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_jpa_hobbies`
--

CREATE TABLE `user_jpa_hobbies` (
  `USER_JPA_ID` bigint(20) NOT NULL,
  `HOBBIES` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `formation_jpa`
--
ALTER TABLE `formation_jpa`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `user_jpa`
--
ALTER TABLE `user_jpa`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `user_jpa_hobbies`
--
ALTER TABLE `user_jpa_hobbies`
  ADD KEY `FKs9lqptwnb4mh9dunlmr1xbpv1` (`USER_JPA_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `formation_jpa`
--
ALTER TABLE `formation_jpa`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
