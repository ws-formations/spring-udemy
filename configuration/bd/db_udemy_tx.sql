-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 30 Septembre 2020 à 21:29
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_udemy_tx`
--

-- --------------------------------------------------------

--
-- Structure de la table `formations_tx`
--

CREATE TABLE `formations_tx` (
  `id` int(11) NOT NULL,
  `titre` varchar(80) DEFAULT NULL,
  `descriptif` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `formations_tx`
--

INSERT INTO `formations_tx` (`id`, `titre`, `descriptif`) VALUES
(1, 'Spring Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. '),
(2, 'Hibernate Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. '),
(3, 'Maven Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. ');

-- --------------------------------------------------------

--
-- Structure de la table `langues`
--

CREATE TABLE `langues` (
  `ID_FORMATION` int(11) DEFAULT NULL,
  `LANGUE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `langues`
--

INSERT INTO `langues` (`ID_FORMATION`, `LANGUE`) VALUES
(2, 'Français'),
(1, 'Anglais'),
(2, 'Anglais'),
(1, 'Français'),
(3, 'Français');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `formations_tx`
--
ALTER TABLE `formations_tx`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `langues`
--
ALTER TABLE `langues`
  ADD KEY `FORMATION_FK` (`ID_FORMATION`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `langues`
--
ALTER TABLE `langues`
  ADD CONSTRAINT `FORMATION_FK` FOREIGN KEY (`ID_FORMATION`) REFERENCES `formations_tx` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
