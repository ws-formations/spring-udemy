#!/bin/sh

#mvn -DTOMCAT_CONF_MYSQL="D:/Documents/DEV/Formations/J2EE/APPLI/spring/udemy/spring-udemy/configuration" install
script_name=$(basename "$0")
echo ${script_name}
script_root=$(readlink -f "$0")
echo ${script_root}
script_root=$(dirname "${script_root}")
echo ${script_root}

# ==============================================

PROFIL=$1


PROJET_DIR="D:/Documents/DEV/Formations/J2EE/APPLI/spring/udemy/spring-udemy/"
#PROJET_DIR="C:/Users/mamadououry.sow/Documents/Formation/J2EE/ws-formations/spring-udemy/"

CONF_DIR_PROD="/opt/tomcat/domain/udemy/configuration"

PROFIL_DEV="dev"

echo 'Repertoire du projet'
echo ${PROJET_DIR}

cd $PROJET_DIR

if [ $PROFIL_DEV = "dev" ]; then
   echo ${CONF_DIR_PROD}
   mvn -Pdev -DTOMCAT_CONF_MYSQL=$CONF_DIR_PROD install
else
   echo ${CONF_DIR_PROD}
   mvn -DTOMCAT_CONF_MYSQL=$CONF_DIR_PROD install
fi

