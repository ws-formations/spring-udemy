package fr.tuto.spring.springcommon.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @version 1.2.0 ajout méthode d'instanciation Logger à partir de son nom
 * @since 1.0.0
 */
public final class LoggerUtils {
    /**
     * classe privee
     */
    private static LoggerUtils singleton = null;

    /**
     * constructeur privee, interdit d'instance.
     */
    private LoggerUtils(){
    }

    /**
     * @return une instance unique de loggerUtils
     */
    public static synchronized LoggerUtils getInstance(){

        if(singleton == null) {
            singleton = new LoggerUtils();
        }

        return singleton;
    }

    /**
     * Méthode permettant d'instancier un logger SLF4J à partir d'une classe.
     * @param nomDeLaClasse Classe de type Class à logger
     * @return un logger slf4j instancier du la Class à logger
     */
    public synchronized Logger getLog(Class<?> nomDeLaClasse){
        Logger retour = null;

        try{
            if(nomDeLaClasse != null) {
                retour = LoggerFactory.getLogger(nomDeLaClasse);
            } else {
                throw new IllegalArgumentException("Class null, impossible d'instancier un logger.");
            }
        } catch (IllegalArgumentException e) {
            retour = getLog(LoggerUtils.class);
            retour.error(e.getMessage());
            retour.warn("Logger instancier avec la classe par defaut : 'LoggerUtils.class'");
        }

        return retour;
    }

    /**
     * Méthode permettant d'instancier un logger SLF4J à partir d'une chaine de caractère
     * @param nomLogger nom du logger
     * @return un logger slf4j instancier pour le nom inqiqué
     */
    public synchronized Logger getLog(String nomLogger){
        Logger retour = null;

        try{
            if(nomLogger != null) {
                retour = LoggerFactory.getLogger(nomLogger);
            } else {
                throw new IllegalArgumentException("Impossible d'instancier un logger sans nom.");
            }
        } catch (IllegalArgumentException e) {
            retour = getLog(LoggerUtils.class);
            retour.error(e.getMessage());
            retour.warn("Logger instancié avec la classe par defaut : 'LoggerUtils.class'");
        }

        return retour;
    }
}
