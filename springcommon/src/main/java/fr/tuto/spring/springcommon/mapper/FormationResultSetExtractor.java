package fr.tuto.spring.springcommon.mapper;

import fr.tuto.spring.springdomaine.modele.Formation;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FormationResultSetExtractor implements ResultSetExtractor<Formation> {

	@Override
	public Formation extractData(final ResultSet rs) throws SQLException, DataAccessException {
		Formation formation = null;
		while (rs.next()) {
			if (formation == null) {
				formation = new Formation(rs.getLong(1), rs.getString(2), rs.getString(3));
			}

			formation.getLangues().add(rs.getString(4));

		}
		return formation;
	}

}
