package fr.tuto.spring.springcommon.mapper;



import fr.tuto.spring.springdomaine.modele.Formation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FormationRowMapper implements RowMapper<Formation> {

	@Override
	public Formation mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		return new Formation(rs.getLong(1), rs.getString(2), rs.getString(3));
	}

}
