package fr.tuto.spring.springcommon.exception;

import java.util.List;

/**
 * Classe d'exception levée quand une erreur technique est survenue
 *
 * @author lgu
 */
public class DAOException extends AbstractApliException {

    public DAOException(String pMessage) {
        super(pMessage);
    }

    public DAOException(String message, List<String> parameters){
        super(message, parameters);
    }
}
