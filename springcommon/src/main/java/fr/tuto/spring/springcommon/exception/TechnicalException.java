package fr.tuto.spring.springcommon.exception;

import java.util.List;

/**
 * Classe d'exception levée quand une erreur technique est survenue
 *
 * @author lgu
 */
public class TechnicalException extends AbstractApliException {

    public TechnicalException(String pMessage) {
        super(pMessage);
    }

    public TechnicalException(String message, List<String> parameters) {
        super(message, parameters);
    }
}
