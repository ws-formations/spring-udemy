package fr.tuto.spring.springcommon.exception;

import java.util.List;

/**
 * Classe d'exception levée quand une erreur technique est survenue
 *
 * @author lgu
 */
public class BusinessException extends AbstractApliException {

    public BusinessException(String pMessage) {
        super(pMessage);
    }

    public BusinessException(String message,  List<String> parameters){
        super(message, parameters);
    }
}
