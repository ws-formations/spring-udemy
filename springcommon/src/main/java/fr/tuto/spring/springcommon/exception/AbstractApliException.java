package fr.tuto.spring.springcommon.exception;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;


public class AbstractApliException extends RuntimeException{

    private final List<String> params = new ArrayList<>();

    public AbstractApliException(List<String> params) {
        super();
        this.params.addAll(params);
    }

    public AbstractApliException(String message) {
        super(message);
    }

    public AbstractApliException(String message, List<String> params) {
        super(message);
        this.params.addAll(params);
    }

    public List<String> getParams(){
        return ImmutableList.copyOf(params);
    }


}
