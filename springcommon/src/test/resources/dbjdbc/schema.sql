drop table if exists formations;
CREATE TABLE formations (
  id         INTEGER PRIMARY KEY,
  titre VARCHAR(80),
  descriptif  VARCHAR(500)
);

