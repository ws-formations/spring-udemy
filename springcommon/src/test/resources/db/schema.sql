drop table if exists formations_jpa;
CREATE TABLE formations_jpa (
  id         INTEGER PRIMARY KEY,
  titre VARCHAR(80),
  descriptif  VARCHAR(500)
);
