package fr.tuto.spring.springwebapi.rest;

import fr.tuto.spring.springwebapi.rest.abstrait.AbstraitController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("app")
public class FormationJdbcTempController extends AbstraitController {

    public FormationJdbcTempController(){
    }

    @GetMapping(value = "/cont")
    public int countAllFormationJpa(){
        return getServiceFactory().getFormationJDBCTemplateService().countJDBCTemFormations();
    }
}
