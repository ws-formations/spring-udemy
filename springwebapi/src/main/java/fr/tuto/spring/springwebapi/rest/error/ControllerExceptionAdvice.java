package fr.tuto.spring.springwebapi.rest.error;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springcommon.exception.DAOException;
import fr.tuto.spring.springcommon.exception.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class ControllerExceptionAdvice {

    private static final Logger log = LoggerFactory.getLogger(ControllerExceptionAdvice.class);

    private static final String MESSAGE_ERREUR_TECHNIQUE = "Une erreur technique à été intercepté: ";
    private static final String MESSAGE_ERREUR_DAO = "Une erreur à été intercepté au niveau de la DAO: ";
    private static final String MESSAGE_ERREUR_API = "Method Not Allowed";
    private static final String MESSAGE_ERREUR_INCONNU = "Erreur inconnue";
    private static final String MESSAGE_ERREUR_FONCTIONNELLE = "Une erreur fonctionnelle à été intercepté au niveau du service: ";
    private static final String MESSAGE_ERREUR_NOTFOUND = "Erreur NotFound";

    private static String getPath(final HttpServletRequest request){
        if(request.getQueryString() != null) {
            return request.getRequestURI() + "?" + request.getQueryString();
        } else {
            return request.getRequestURI();
        }
    }

    private static String getCurrentTime(){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    private static ResponseEntity<RestError> exceptionHandler(final RestError restError, HttpStatus httpStatus){
        return ResponseEntity.status(httpStatus).body(restError);
    }

    @ResponseBody
    @ExceptionHandler(TechnicalException.class)
    public ResponseEntity<RestError> handleTechnicalException(final HttpServletRequest httpRequest, final TechnicalException techEx) {
        RestError restapi = new RestError(BAD_REQUEST.value(), MESSAGE_ERREUR_TECHNIQUE, techEx.getParams() ,getCurrentTime(), getPath(httpRequest));
        log.error(MESSAGE_ERREUR_TECHNIQUE, techEx);
        return exceptionHandler(restapi, BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(DAOException.class)
    public ResponseEntity<RestError> handleBusinessException(final HttpServletRequest httpRequest, final DAOException daoEx) {
        RestError restapi = new RestError(BAD_REQUEST.value(), MESSAGE_ERREUR_DAO.concat(daoEx.getMessage()), daoEx.getParams() ,getCurrentTime(), getPath(httpRequest));
        log.error(MESSAGE_ERREUR_DAO.concat(daoEx.getMessage()), daoEx);
        return exceptionHandler(restapi, BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<RestError> handleBusinessException(final HttpServletRequest httpRequest, final BusinessException busEx) {
        RestError restapi = new RestError(BAD_REQUEST.value(), MESSAGE_ERREUR_FONCTIONNELLE.concat(busEx.getMessage()), busEx.getParams() ,getCurrentTime(), getPath(httpRequest));
        log.error(MESSAGE_ERREUR_FONCTIONNELLE.concat(busEx.getMessage()), busEx);
        return exceptionHandler(restapi, BAD_REQUEST);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<RestError> handleNumberFormatException(final HttpServletRequest httpRequest, NumberFormatException nbFormatEx) {
        RestError restapi = new RestError(BAD_REQUEST.value(), MESSAGE_ERREUR_TECHNIQUE.concat(nbFormatEx.getMessage()), getCurrentTime(), getPath(httpRequest));
        log.error(MESSAGE_ERREUR_TECHNIQUE, nbFormatEx);
        return exceptionHandler(restapi, BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseEntity<>(getBody(BAD_REQUEST, ex, ex.getMessage()), new HttpHeaders(), BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        return new ResponseEntity<>(getBody(FORBIDDEN, ex, ex.getMessage()), new HttpHeaders(), FORBIDDEN);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        return new ResponseEntity<>(getBody(INTERNAL_SERVER_ERROR, ex, "Something Went Wrong"), new HttpHeaders(), INTERNAL_SERVER_ERROR);
    }


    public Map<String, Object> getBody(HttpStatus status, Exception ex, String message) {
        log.error(message, ex);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", message);
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("exception", ex.toString());

        Throwable cause = ex.getCause();
        if (cause != null) {
            body.put("exceptionCause", ex.getCause().toString());
        }
        return body;
    }


}
