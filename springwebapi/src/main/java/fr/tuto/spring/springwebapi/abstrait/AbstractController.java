package fr.tuto.spring.springwebapi.abstrait;

import fr.tuto.spring.springservice.factory.ServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public abstract class AbstractController{

    @Autowired
    private ServiceFactory serviceFactory;

    public ServiceFactory getServiceFactory(){
        return serviceFactory;
    }
}
