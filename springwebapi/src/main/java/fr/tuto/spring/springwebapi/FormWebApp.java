package fr.tuto.spring.springwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@SpringBootConfiguration
@ComponentScan(basePackages = {"fr.tuto.spring"})
public class FormWebApp {

	public static void main(String[] args) {
		SpringApplication.run(FormWebApp.class);
	}
}
