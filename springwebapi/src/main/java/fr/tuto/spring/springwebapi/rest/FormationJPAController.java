package fr.tuto.spring.springwebapi.rest;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springwebapi.rest.abstrait.AbstraitController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("app")
public class FormationJPAController extends AbstraitController {

    public FormationJPAController(){
    }

	 @PostMapping(path = "/jpa/formation", produces = {"application/json"})
	 public ResponseEntity<FormationJpa> createFormationJpa(@RequestBody FormationJpa form) throws BusinessException {
			FormationJpa newFormationJpa;
			try {
				 newFormationJpa = getServiceFactory().getFormJpaEMService().createFormationJpa(form);
			}catch (BusinessException e) {
				 throw new BusinessException("Impossible de créer la formation par le service Formation");
			}
			return new ResponseEntity<>(newFormationJpa, HttpStatus.OK);
	 }

    @GetMapping(path = "/jpa/cont")
    public Long countAllFormationJpa(){
        return getServiceFactory().getFormJpaEMService().countAllFormationJpa();
    }

    @GetMapping(path = "/jpa/formation/{idForm}", produces = {"application/json"})
    public ResponseEntity<FormationJpa> findFormationJpa(@PathVariable Long idForm) throws BusinessException {
        FormationJpa formJpa;
        try {
            formJpa = getServiceFactory().getFormJpaEMService().findFormationJpa(idForm);
        }catch (BusinessException e) {
            throw new BusinessException("Impossible de recupérer la formation par le service Formation Pour ID = "+idForm);
        }
        return new ResponseEntity<>(formJpa, HttpStatus.OK);
    }

    @GetMapping(path = "/jpa/formations", produces = {"application/json"})
    public ResponseEntity<List<FormationJpa>> findAllFormationJpa() {
        List<FormationJpa> formationJpaList;
        try {
            formationJpaList = getServiceFactory().getFormJpaEMService().findAllFormationJpa();
        }catch (BusinessException e) {
            throw new BusinessException("Impossible de recupérer la liste des formation par le service Formation");
        }
        return new ResponseEntity<>(formationJpaList, HttpStatus.OK);
    }
}
