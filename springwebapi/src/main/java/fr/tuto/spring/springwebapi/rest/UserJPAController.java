package fr.tuto.spring.springwebapi.rest;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springdomaine.jpa.UserJpa;
import fr.tuto.spring.springwebapi.rest.abstrait.AbstraitController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(value = "*")
@RestController
@RequestMapping("app")
public class UserJPAController extends AbstraitController {

    public UserJPAController(){
    }

    @GetMapping(path = "/user/cont")
    public Long countAllUserJpa(){
        return getServiceFactory().getUserJpaService().getCountUserJpa();
    }

    @GetMapping(path = "/user/{idUser}", produces = {"application/json"})
    public ResponseEntity<UserJpa> findUserJpa(@PathVariable Long idUser) throws BusinessException {
        UserJpa userJpa;
        try {
            userJpa = getServiceFactory().getUserJpaService().findUserJpaParID(idUser);
        }catch (BusinessException e) {
            throw new BusinessException("Impossible de recupérer un userJpa par le service userJpa Pour ID = "+idUser);
        }
        return new ResponseEntity<>(userJpa, HttpStatus.OK);
    }

    @GetMapping(path = "/user/users", produces = {"application/json"})
    public ResponseEntity<List<UserJpa>> findAllUserJpaJpa() {
        List<UserJpa> userJpaList;
        try {
            userJpaList = getServiceFactory().getUserJpaService().findAllUsersJpa();
        }catch (BusinessException e) {
            throw new BusinessException("Impossible de recupérer la liste des userJpa par le service userJpa");
        }
        return new ResponseEntity<>(userJpaList, HttpStatus.OK);
    }
}
