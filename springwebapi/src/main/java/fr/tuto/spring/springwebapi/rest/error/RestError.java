package fr.tuto.spring.springwebapi.rest.error;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RestError {

    private final Integer httpStatus;
    private final String message;
    private final List<String> errorParameters;
    private final String timestamp;
    private final String path;

    /**
     Instancie une erreur API, qui est fournie en réponse de l'appel.
     {@link #getHttpStatus() getHttpStatus})
     @param httpStatus code HTTP
     @param message message de l'exception (facultatif)
     @param errorParameters liste de paramètres associés au code erreur fonctionnelle
     @param timestamp timestamp de l'exception
     @param path path HTTP de l'appel en erreur
     */
    public RestError(Integer httpStatus, String message, List<String> errorParameters, String timestamp, String path){
        this.httpStatus = httpStatus;
        this.message = message;
        this.errorParameters = Objects.nonNull(errorParameters)? ImmutableList.copyOf(errorParameters) : new ArrayList<>();
        this.timestamp = timestamp;
        this.path = path;
    }

    public RestError(Integer httpStatus, String message, String timestamp, String path){
       this(httpStatus, message, null, timestamp, path);
    }

    public Integer getHttpStatus(){
        return httpStatus;
    }

    public String getMessage(){
        return message;
    }

    public String getTimestamp(){
        return timestamp;
    }

    public String getPath(){
        return path;
    }

    public List<String> getErrorParameters(){
        return ImmutableList.copyOf(errorParameters);
    }
}
