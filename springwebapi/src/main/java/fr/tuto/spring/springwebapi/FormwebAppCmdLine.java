package fr.tuto.spring.springwebapi;

import fr.tuto.spring.springcommon.logger.LoggerUtils;
import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springjdbcdao.factory.DaoJDBCFactory;
import fr.tuto.spring.springjdbcdao.jdbctemplate.FormJDBCTemplateDao;
import fr.tuto.spring.springjdbcdao.namedparamjdbctemplate.FormNamedParamJDBCTemplateDao;
import fr.tuto.spring.springjpadao.factory.DaoJPAFactory;
import fr.tuto.spring.springjpadao.jpa.FormationJpaDao;
import fr.tuto.spring.springjpadao.jpa.UserJpaDao;
import fr.tuto.spring.springservice.factory.ServiceFactory;
import fr.tuto.spring.springservice.jdbc.namedparam.FormNamedParamJDBCTemplateService;
import fr.tuto.spring.springservice.jdbc.template.FormJDBCTemplateService;
import fr.tuto.spring.springservice.jpa.FormJpaEMService;
import fr.tuto.spring.springservice.jpa.UserJpaService;
import fr.tuto.spring.springservice.tx.FormTxNamedParameterJDBCService;
import fr.tuto.spring.springtxdao.factory.DaoTxFactory;
import fr.tuto.spring.springtxdao.jdbctx.FormTxNamedParameterJDBCDao;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FormwebAppCmdLine implements CommandLineRunner{

	private static final Logger  log = LoggerUtils.getInstance().getLog(FormwebAppCmdLine.class);

	@Autowired
	private ApplicationContext ctx;

	@Override
	public void run(String... args) {
		//getBeanJpaFromDao();
		getBeanUserServiceDao();
	}
	private void getBeanUserServiceDao() {
		log.info("------------- START WEB RUN --------------------");
		ServiceFactory serviceFactory = ctx.getBean(ServiceFactory.class);

		UserJpaService jpaEMService = serviceFactory.getUserJpaService();

		//UserJpa userJpa = new UserJpa("/assets/mesimages/coeur.jpg", "Coeur Tessi", "BAH", "jaques-tes@outlook.com", "Orange", Arrays.asList("Sport","Tennis"));
		//UserJpa newUser = jpaEMService.createUserJpa(userJpa);
		//System.out.println("UserJpa JPA SERVICE = "+newUser);

		long nbjpaf = jpaEMService.getCountUserJpa();
		System.out.println("NB UserJpa Jpa DAO = "+nbjpaf);

		log.info("------------- START WEB RUN --------------------");
		DaoJPAFactory daoJPAFactory = ctx.getBean(DaoJPAFactory.class);
		UserJpaDao userJpaDao = daoJPAFactory.getUserJpaDao();

		jpaEMService.findAllUsersJpa().forEach(userJpa2 -> {
					System.out.println("UserJpa JPA SERVICE = "+userJpa2);
					if (userJpa2!=null) {
						//	jpaEMService.deleteUserJpa(userJpa2);
					}
				}
		);

		//UserJpa userJpa1 = userJpaDao.findUserJpaByID(11L);
		//System.out.println("UserJpa JPA SERVICE = "+userJpa1);

	}

	private void getBeanJpaFromDao() {
		log.info("------------- START WEB RUN --------------------");
		DaoJPAFactory daoJPAFactory = ctx.getBean(DaoJPAFactory.class);
		FormationJpaDao formationJpaDao = daoJPAFactory.getFormationJpaDao();

		FormationJpa formationJpa = formationJpaDao.createFormationJpa(new FormationJpa("J2EE MVC: étape par étape pour devenir professionnel", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis"));
		System.out.println("Foramtion Jpa DAO = "+formationJpa);
		long nbjpaf = formationJpaDao.getCountFormJpa();
		System.out.println("NB Foramtion Jpa DAO = "+nbjpaf);
	}

	private void getBeanFromDao() {
		log.info("------------- START WEB RUN --------------------");

		DaoJPAFactory daoJPAFactory = ctx.getBean(DaoJPAFactory.class);
		FormationJpaDao formationJpaDao = daoJPAFactory.getFormationJpaDao();
		long nbjpaf = formationJpaDao.getCountFormJpa();
		System.out.println("NB Foramtion Jpa DAO = "+nbjpaf);

		DaoJDBCFactory daoJDBCFactory = ctx.getBean(DaoJDBCFactory.class);
		FormJDBCTemplateDao formJDBCTemplateDao = daoJDBCFactory.getFormJDBCTemplateDao();
		int nbf = formJDBCTemplateDao.countJDBCTemFormations();
		System.out.println("NB Formation JDBC Template DAO = "+ nbf);

		FormNamedParamJDBCTemplateDao formationNamed = daoJDBCFactory.getFormNamedParamJDBCTemplateDao();
		int nbfNamed = formationNamed.countFormJdbcTempNamed();
		System.out.println("NB Formation JDBC Named Parameter DAO  = "+ nbfNamed);

		DaoTxFactory daoTxFactory = ctx.getBean(DaoTxFactory.class);
		FormTxNamedParameterJDBCDao formationNamedTx = daoTxFactory.getFormTxNamedParameterJDBCDao();
		int nbformNamedTx = formationNamedTx.countTxFormations();
		System.out.println("NB Formation JDBC Named Parameter TX DAO  = "+ nbformNamedTx);

		log.info("---------- END RUN WEB APP -------");
	}

	private void getBeanFromService() {
		log.info("------------- START WEB RUN --------------------");

		ServiceFactory serviceFactory = ctx.getBean(ServiceFactory.class);

		FormJpaEMService jpaEMService = serviceFactory.getFormJpaEMService();
		long nbjpaf = jpaEMService.countAllFormationJpa();
		System.out.println("NB Form JPA SERVICE = "+nbjpaf);

		FormJDBCTemplateService jdbcTemplateService = serviceFactory.getFormationJDBCTemplateService();
		int nbf = jdbcTemplateService.countJDBCTemFormations();
		System.out.println("NB Form JDBC TEMPLATE SERVICE = "+ nbf);

		FormTxNamedParameterJDBCService formTxNamedParameterJDBCService = serviceFactory.getFormTxNamedParameterJDBCService();
		int nbformNamedTx = formTxNamedParameterJDBCService.countTxFormations();
		System.out.println("NB Formation JDBC Named Parameter TX DAO  = "+ nbformNamedTx);

		FormNamedParamJDBCTemplateService namedParamJDBCTemplateService = serviceFactory.getFormNamedParamJDBCTemplateService();
		int nbfNamed = namedParamJDBCTemplateService.countFormJdbcTempNamed();
		System.out.println("NB Formation JDBC Named Parameter DAO  = "+ nbfNamed);

		log.info("---------- END RUN WEB APP -------");
	}
}
