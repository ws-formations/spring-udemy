package fr.tuto.spring.springjdbcdao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@EnableAutoConfiguration
@ComponentScan(basePackages = { "fr.tuto.spring.springjdbcdao" })
@SpringBootConfiguration
public class DaoApliTest {

    public static void main(String[] args) {
        SpringApplication.run(DaoApliTest.class);
    }
}
