package fr.tuto.spring.springjdbcdao.jdbctemplate;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.DaoApliTest;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { DaoApliTest.class})
public class FormJDBCTemplateDaoTest {

	@Autowired
	private FormJDBCTemplateDao formJDBCTemplateDao;

	@Test
	@Transactional
	@Rollback
	public void test1_CreateRunWithoutError() {
		Formation formation = new Formation(5L,"formation 1", "descriptif");
		formJDBCTemplateDao.createFormJdbcTemp(formation);
		 assertThat(formJDBCTemplateDao.findAllFormJdbcTemp()).hasSize(5);
		assertThat(formJDBCTemplateDao.countJDBCTemFormations()).isPositive();
		formJDBCTemplateDao.findAllFormJdbcTemp().forEach(form -> System.out.println("Mes Formation " +form.toString()));
	}

	 @BeforeTransaction
	 public void beforeTx() {
			assertThat(formJDBCTemplateDao.findAllFormJdbcTemp()).hasSize(4);
	 }

	 @AfterTransaction
	 public void afterTx() {
			assertThat(formJDBCTemplateDao.findAllFormJdbcTemp()).hasSize(4);
	 }

	 @Test
	 public void test2_findAllFormJdbcTemp() {
			assertThat(formJDBCTemplateDao.findAllFormJdbcTemp()).hasSize(4);
	 }
}
