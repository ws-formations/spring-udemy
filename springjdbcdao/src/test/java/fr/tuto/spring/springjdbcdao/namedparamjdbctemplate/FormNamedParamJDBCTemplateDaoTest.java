package fr.tuto.spring.springjdbcdao.namedparamjdbctemplate;
import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.DaoApliTest;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { DaoApliTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FormNamedParamJDBCTemplateDaoTest {

    @Autowired
    private FormNamedParamJDBCTemplateDao beanUnderTest;

    @Test
    @Transactional
    @Rollback
    public void should1FindNewlyCreatedFormation() {
        Formation formation = new Formation(5l, "nouvelle formation", "un descriptif");
        beanUnderTest.createFormJdbcTempNamed(formation);

        assertThat(beanUnderTest.findFormJdbcTempNamedParId(5l)).isNotNull();
    }

    @BeforeTransaction
    public void beforeTx() {
        assertThat(beanUnderTest.findAllFormJdbcTempNamed()).hasSize(4);
    }

    @AfterTransaction
    public void afterTx() {
        assertThat(beanUnderTest.findAllFormJdbcTempNamed()).hasSize(4);
    }

    @Test
    // @Sql(scripts = "classpath:db/nouvelle-formation.sql")
    @SqlGroup({ @Sql(statements = { "INSERT INTO formations_jdbc VALUES (5, 'nouvelle formation', 'descriptif');" }) })
    public void shouldFindTitreByIdReturn() {
        assertThat(beanUnderTest.findTitreFormJdbcTempNamedParId(5L)).isEqualTo("nouvelle formation");
    }

    @Test
    public void should2FindAllReturn3Items() {
        assertThat(beanUnderTest.findAllFormJdbcTempNamed()).hasSize(4);
    }
}
