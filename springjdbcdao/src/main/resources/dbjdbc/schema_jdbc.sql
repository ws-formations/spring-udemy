drop table if exists langues;
drop table if exists formations_jdbc;
CREATE TABLE formations_jdbc (
  id         INTEGER PRIMARY KEY,
  titre VARCHAR(80),
  descriptif  VARCHAR(500)
);

CREATE TABLE langues (
  ID_FORMATION int(11) DEFAULT NULL,
  LANGUE varchar(50) DEFAULT NULL
);

ALTER TABLE langues ADD CONSTRAINT FORMATION_FK FOREIGN KEY (ID_FORMATION) REFERENCES formations_jdbc (ID);


