INSERT INTO formations_jdbc VALUES (1, 'DAO Spring Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. ');
INSERT INTO formations_jdbc VALUES (2, 'DAO Hibernate Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. ');
INSERT INTO formations_jdbc VALUES (3, 'DAO Maven Framework: étape par étape pour devenir professionnel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. ');
INSERT INTO formations_jdbc VALUES (4, 'DAO Maven Hibernate', 'Lorem Hibernate ipsum dolor sit amet, consectetur adipiscing elit. Proin ut eros accumsan, tempus justo id, feugiat felis. Nullam pretium velit urna, dictum aliquam augue mattis sed. Maecenas eu magna volutpat. ');


INSERT INTO langues VALUES (2, 'Français');
INSERT INTO langues VALUES (1, 'Anglais');
INSERT INTO langues VALUES (2, 'Anglais');
INSERT INTO langues VALUES (1, 'Français');
INSERT INTO langues VALUES (3, 'Français');
