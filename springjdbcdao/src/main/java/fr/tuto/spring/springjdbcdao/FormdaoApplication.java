package fr.tuto.spring.springjdbcdao;

import fr.tuto.spring.springtechnique.confds.DaoTechnicalProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class FormdaoApplication implements CommandLineRunner{

	@Autowired
	DaoTechnicalProperties daoProperties;

	private static final Logger log = LoggerFactory.getLogger(FormdaoApplication.class);

	@Override
	public void run(String... args) {
		log.info("---------- RUN JDBC DAO ----------");
	}
}
