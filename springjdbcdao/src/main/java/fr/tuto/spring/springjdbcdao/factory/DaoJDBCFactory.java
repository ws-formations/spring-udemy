package fr.tuto.spring.springjdbcdao.factory;

import fr.tuto.spring.springjdbcdao.jdbctemplate.FormJDBCTemplateDao;
import fr.tuto.spring.springjdbcdao.namedparamjdbctemplate.FormNamedParamJDBCTemplateDao;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public interface DaoJDBCFactory{

    FormJDBCTemplateDao getFormJDBCTemplateDao();

    FormNamedParamJDBCTemplateDao getFormNamedParamJDBCTemplateDao();
}
