package fr.tuto.spring.springjdbcdao.namedparamjdbctemplate;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

public class BatchCreateFormation extends BatchSqlUpdate {

	private static final String QUERY = "INSERT INTO formations values(:id,:titre,:descriptif)";

	BatchCreateFormation(final DataSource dataSource) {
		super(dataSource, QUERY);

		declareParameter(new SqlParameter(Types.INTEGER, "id"));
		declareParameter(new SqlParameter(Types.VARCHAR, "titre"));
		declareParameter(new SqlParameter(Types.VARCHAR, "descriptif"));
	}
}
