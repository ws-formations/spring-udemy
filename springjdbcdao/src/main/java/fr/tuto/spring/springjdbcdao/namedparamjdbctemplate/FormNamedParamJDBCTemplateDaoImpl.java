package fr.tuto.spring.springjdbcdao.namedparamjdbctemplate;

import fr.tuto.spring.springcommon.mapper.FormationResultSetExtractor;
import fr.tuto.spring.springcommon.mapper.FormationRowCallBackHandler;
import fr.tuto.spring.springcommon.mapper.FormationRowMapper;
import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.abstrait.AbstractJDBC;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FormNamedParamJDBCTemplateDaoImpl extends AbstractJDBC implements FormNamedParamJDBCTemplateDao {

	@Override
	public int countFormJdbcTempNamed() {
		return getNamedParameterJdbcTemplate().queryForObject("Select count(*) from formations_jdbc ", new HashMap<>(), Integer.class);

	}

	// ResultSetExtractor
	@Override
	public Formation findAvecLangues(final Long id) {
		String query = "select f.id, f.titre, f.descriptif, l.langue from formations_jdbc f , langues l where f.id= l.id_formation and f.id=:id";
		Map<String, Object> params = new HashMap<>();
		params.put("id", id);

		return getNamedParameterJdbcTemplate().query(query, params, new FormationResultSetExtractor());
	}

	// RowCallbackHandler
	@Override
	public void extractToCsvFormJdbcTempNamed() {
		getNamedParameterJdbcTemplate().query("select * from formations_jdbc", new FormationRowCallBackHandler());

	}

	// RowMapper
	@Override
	public List<Formation> findAllFormJdbcTempNamed() {
		return getNamedParameterJdbcTemplate().query("select * from formations_jdbc", new FormationRowMapper());
	}

	@Override
	public void createFormJdbcTempNamed(final Formation formation) {
		String query = "INSERT INTO formations_jdbc values(:id,:titre,:descriptif)";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", formation.getId());
		mapSqlParameterSource.addValue("titre", formation.getTitre());
		mapSqlParameterSource.addValue("descriptif", formation.getDescriptif());
		getNamedParameterJdbcTemplate().update(query, mapSqlParameterSource);
	}

	@Override
	public void updateFormJdbcTempNamed(final Formation formation) {

		String query = "Update formations_jdbc set titre=:titre ,descriptif=:descriptif where id=:id";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", formation.getId());
		mapSqlParameterSource.addValue("titre", formation.getTitre());
		mapSqlParameterSource.addValue("descriptif", formation.getDescriptif());
		getNamedParameterJdbcTemplate().update(query, mapSqlParameterSource);
	}

	@Override
	public void deleteFormJdbcTempNamed(final Formation formation) {
		String query = "delete from formations_jdbc where id=:id";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", formation.getId());
		getNamedParameterJdbcTemplate().update(query, mapSqlParameterSource);
	}

	@Override
	public Formation findFormJdbcTempNamedParId(final Long id) {
		String query = "Select * from formations_jdbc where id=:id";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", id);
		return getNamedParameterJdbcTemplate().query(query, mapSqlParameterSource, new FormationRowMapper()).get(0);
	}

	@Override
	public int countFormJdbcTempNamedParLangue(final String langue) {
		String query = "select count(*) from formations_jdbc f , langues l where f.id= l.id_formation and l.langue=:langue";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("langue", langue);

		return getNamedParameterJdbcTemplate().queryForObject(query, mapSqlParameterSource, Integer.class);

	}

	@Override
	public String findTitreFormJdbcTempNamedParId(final Long id) {
		String query = "Select titre from formations_jdbc where id=:id";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", id);

		return getNamedParameterJdbcTemplate().queryForObject(query, mapSqlParameterSource, String.class);
	}

	@Override
	public void createFormJdbcTempNamed(final Collection<Formation> formations) {
		BatchCreateFormation batchCreateFormation = new BatchCreateFormation(getDataSource());

		for (Formation formation : formations) {
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("id", formation.getId());
			paramMap.put("titre", formation.getTitre());
			paramMap.put("descriptif", formation.getDescriptif());
			batchCreateFormation.updateByNamedParam(paramMap);
		}
	}

}
