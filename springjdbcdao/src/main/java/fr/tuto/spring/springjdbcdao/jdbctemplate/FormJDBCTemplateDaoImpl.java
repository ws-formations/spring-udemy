package fr.tuto.spring.springjdbcdao.jdbctemplate;

import fr.tuto.spring.springcommon.mapper.FormationResultSetExtractor;
import fr.tuto.spring.springcommon.mapper.FormationRowCallBackHandler;
import fr.tuto.spring.springcommon.mapper.FormationRowMapper;
import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.abstrait.AbstractJDBC;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>Cette class utilise le bean JdbcTemplate de DaoJdbcConfig </p>
 * Bean declaré dans DaoJdbcConfig {@link JdbcTemplate}
 *
 */
@Repository
public class FormJDBCTemplateDaoImpl extends AbstractJDBC implements FormJDBCTemplateDao {

	@Override
	public int countJDBCTemFormations() {
		return getJdbcTemplate().queryForObject("Select count(*) from formations_jdbc ", Integer.class);
	}

	//@Override
	public Formation fgindFormJdbcTempParId(final Long id) {
		String query = "Select f.id, f.titre, f.descriptif from formations_jdbc f where f.id=?";
		return getJdbcTemplate().queryForObject(query, new Object[] {id}, Formation.class);
	}

	@Override
	public Formation findFormJdbcTempParId(final Long id) {
		String query = "Select f.id, f.titre, f.descriptif from formations_jdbc f where f.id=?";
		return getJdbcTemplate().queryForObject(query, new Object[] {id},  new FormationRowMapper());
	}

	// ResultSetExtractor
	@Override
	public Formation findFormJdbcTempAvecLangues(final Long id) {
		String query = "select f.id, f.titre, f.descriptif, l.langue from formations_jdbc f , langues l where f.id= l.id_formation and f.id="
				+ id;
		return getJdbcTemplate().query(query, new FormationResultSetExtractor());
	}

	// RowCallbackHandler
	@Override
	public void extractJdbcTempFormToCsv() {
		getJdbcTemplate().query("select * from formations_jdbc", new FormationRowCallBackHandler());

	}

	// RowMapper
	@Override
	public List<Formation> findAllFormJdbcTemp() {
		return getJdbcTemplate().query("select * from formations_jdbc", new FormationRowMapper());
	}

	@Override
	public void createFormJdbcTemp(final Formation formation) {
		String query = "INSERT INTO formations_jdbc values(?,?,?)";
		getJdbcTemplate().update(query, formation.getId(), formation.getTitre(), formation.getDescriptif());
	}

	@Override
	public void updateFormJdbcTemp(final Formation formation) {

		String query = "Update formations_jdbc set titre=? , descriptif=? where id=?";
		getJdbcTemplate().update(query, formation.getTitre(), formation.getDescriptif(), formation.getId());
	}

	@Override
	public void deleteFormJdbcTemp(final Formation formation) {
		String query = "delete from formations_jdbc where id=?";
		getJdbcTemplate().update(query, formation.getId());
	}

}
