package fr.tuto.spring.springjdbcdao.namedparamjdbctemplate;

import fr.tuto.spring.springdomaine.modele.Formation;

import java.util.Collection;
import java.util.List;

/**
 Created By Mamadou Oury SOW On 20/09/2020 **/
public interface FormNamedParamJDBCTemplateDao {

    int countFormJdbcTempNamed();

    // ResultSetExtractor
    Formation findAvecLangues(Long id);

    // RowCallbackHandler
    void extractToCsvFormJdbcTempNamed();

    // RowMapper
    List<Formation> findAllFormJdbcTempNamed();

    void createFormJdbcTempNamed(Formation formation);

    void updateFormJdbcTempNamed(Formation formation);

    void deleteFormJdbcTempNamed(Formation formation);

    Formation findFormJdbcTempNamedParId(Long id);

    int countFormJdbcTempNamedParLangue(String langue);

    String findTitreFormJdbcTempNamedParId(Long id);

    void createFormJdbcTempNamed(Collection<Formation> formations);
}
