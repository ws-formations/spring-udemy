package fr.tuto.spring.springjdbcdao.confjdbc;

import fr.tuto.spring.springtechnique.confds.DaoJDBCTechnicalProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;


/**
 * @ImportResource({"classpath:dbjdbc/schema_jpa.sql","classpath:dbjdbc/data_jpa.sql"})
 */
@Configuration
@ComponentScan(basePackages = {"fr.tuto.spring.springjdbcdao", "fr.tuto.spring.springtechnique" })
public class DaoJdbcConfig{

	private static final Logger log = LoggerFactory.getLogger(DaoJdbcConfig.class);

	@Autowired
    private DaoJDBCTechnicalProperties daoJDBCTechnicalProperties;

	/**
	 * On crée un bean DataSource qui sera utilisé par JDBC Template JdbcTemplate
	 * @see JdbcTemplate
	 * @return DataSource
	 */
	@Bean(name = "daoDataSource")
	@Profile("!test")
	public DataSource daoDataSource() {
		log.info("URRL Module JPA DAO = "+ daoJDBCTechnicalProperties.getUrlmysql("JDBC"));
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(daoJDBCTechnicalProperties.getDbdriver());
		dataSource.setUrl(daoJDBCTechnicalProperties.getUrlmysql("JDBC"));
		dataSource.setUsername(daoJDBCTechnicalProperties.getUser());
		dataSource.setPassword(daoJDBCTechnicalProperties.getPassword());
		return dataSource;
	}
	/**
	 * On crée un bean DataSource qui sera utilisé par JDBC Template
	 *@See JdbcTemplate
	 * @return DataSource
	 */

	 @Bean(name = "daoDataSource")
	 @Profile("test")
	 public DataSource daoDataSourceH2() {
	 	return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).addScript("classpath:dbjdbc/schema_jdbc.sql")
	 			.addScript("classpath:dbjdbc/data_jdbc.sql").build();
	 }

	@Profile("!test")
	@Bean(name = "daoJdbcTemplate")
	public JdbcTemplate daoJdbcTemplate(@Qualifier("daoDataSource") final DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Profile("test")
	@Bean(name = "daoJdbcTemplate")
	public JdbcTemplate daoJdbcTemplateH2(@Qualifier("daoDataSource") final DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Profile("!test")
	@Bean(name = "daoNamedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate daoNamedParameterJdbcTemplate(@Qualifier("daoDataSource") final DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Profile("test")
	@Bean(name = "daoNamedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate daoNamedParameterJdbcTemplateH2(@Qualifier("daoDataSource") final DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

}
