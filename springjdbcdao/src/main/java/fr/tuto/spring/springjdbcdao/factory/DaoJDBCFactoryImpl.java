package fr.tuto.spring.springjdbcdao.factory;

import fr.tuto.spring.springjdbcdao.jdbctemplate.FormJDBCTemplateDao;
import fr.tuto.spring.springjdbcdao.namedparamjdbctemplate.FormNamedParamJDBCTemplateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DaoJDBCFactoryImpl implements DaoJDBCFactory{

    @Autowired
    private FormJDBCTemplateDao formJDBCTemplateDao;

    @Autowired
    private FormNamedParamJDBCTemplateDao formNamedParamJDBCTemplateDao;

    @Override
    public FormJDBCTemplateDao getFormJDBCTemplateDao(){
        return formJDBCTemplateDao;
    }

    @Override
    public FormNamedParamJDBCTemplateDao getFormNamedParamJDBCTemplateDao(){
        return formNamedParamJDBCTemplateDao;
    }
}
