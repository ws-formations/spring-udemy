package fr.tuto.spring.springjdbcdao.jdbctemplate;


import fr.tuto.spring.springdomaine.modele.Formation;

import java.util.List;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public interface FormJDBCTemplateDao {

    int countJDBCTemFormations();

    Formation findFormJdbcTempParId(Long id);

    // ResultSetExtractor
    Formation findFormJdbcTempAvecLangues(Long id);

    // RowCallbackHandler
    void extractJdbcTempFormToCsv();

    // RowMapper
    List<Formation> findAllFormJdbcTemp();

    void createFormJdbcTemp(Formation formation);

    void updateFormJdbcTemp(Formation formation);

    void deleteFormJdbcTemp(Formation formation);
}
