package fr.tuto.spring.springjdbcdao.abstrait;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public abstract class AbstractJDBC{

    @Autowired
    @Qualifier("daoDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("daoJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("daoNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public DataSource getDataSource(){
        return dataSource;
    }

    public JdbcTemplate getJdbcTemplate(){
        return jdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
        return namedParameterJdbcTemplate;
    }
}
