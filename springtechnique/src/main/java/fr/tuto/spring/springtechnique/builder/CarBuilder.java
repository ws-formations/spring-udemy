package fr.tuto.spring.springtechnique.builder;

import java.util.ArrayList;
import java.util.List;

public class CarBuilder {
    private Long id;
    private String titre;
    private String descriptif;
    private List<String> langues = new ArrayList<>();

    public Car build(){
        return new Car(this);
    }

    public CarBuilder id(Long id){
        this.id = id;
        return this;
    }

    public CarBuilder titre(String titre){
        this.titre = titre;
        return this;
    }

    public CarBuilder langues(List<String> langues){
        this.langues = langues;
        return this;
    }

    public CarBuilder descriptif(String descriptif){
        this.descriptif = descriptif;
        return this;
    }

    public CarBuilder idAndTitre(Long id, String titre){
        this.id = id;
        this.titre = titre;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public List<String> getLangues() {
        return langues;
    }
}
