package fr.tuto.spring.springtechnique;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class TechnicalAPP implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(TechnicalAPP.class);

	@Override
	public void run(String... args) {
		log.info("---------- RUN TECHNICAL APP ----------");
		log.info("----------SYSTEM PROPERTIES V CONF_MYSQL ----------" + System.getenv("TOMCAT_CONF_MYSQL"));
	}
}

