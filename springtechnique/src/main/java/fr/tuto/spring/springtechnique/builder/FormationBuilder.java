package fr.tuto.spring.springtechnique.builder;

import java.util.ArrayList;
import java.util.List;

public class FormationBuilder {

    private Long id;
    private String titre;
    private String descriptif;
    private List<String> langues;

    private FormationBuilder(Builder builder){
        this.id = builder.id;
        this.titre = builder.titre;
        this.descriptif = builder.descriptif;
        this.langues = builder.langues;
    }

    static class Builder{
        private Long id;
        private String titre;
        private String descriptif;
        private List<String> langues = new ArrayList<>();

        public FormationBuilder build(){
            return new FormationBuilder(this);
        }

        public Builder id(Long id){
            this.id = id;
            return this;
        }

        public Builder titre(String titre){
            this.titre = titre;
            return this;
        }

        public Builder descriptif(String descriptif){
            this.descriptif = descriptif;
            return this;
        }

        public Builder idAndTitre(Long id, String titre){
            this.id = id;
            this.titre = titre;
            return this;
        }
    }

    public Long getId(){
        return id;
    }

    public String getTitre(){
        return titre;
    }

    public String getDescriptif(){
        return descriptif;
    }

    public List<String> getLangues(){
        return langues;
    }

    @Override
    public String toString() {
        return "Formation [id=" + id + ", titre=" + titre + ", descriptif=" + descriptif + "]";
    }

}
