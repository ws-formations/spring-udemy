package fr.tuto.spring.springtechnique.builder;

import java.util.List;

public class Car {

    private Long id;
    private String titre;
    private String descriptif;
    private List<String> langues;

    public Car(CarBuilder builder){
        this.id = builder.getId();
        this.titre = builder.getTitre();
        this.descriptif = builder.getDescriptif();
        this.langues = builder.getLangues();
    }

    public Long getId(){
        return id;
    }

    public String getTitre(){
        return titre;
    }

    public String getDescriptif(){
        return descriptif;
    }

    public List<String> getLangues(){
        return langues;
    }

    @Override
    public String toString() {
        return "Formation [id=" + id + ", titre=" + titre + ", descriptif=" + descriptif + "]";
    }
}
