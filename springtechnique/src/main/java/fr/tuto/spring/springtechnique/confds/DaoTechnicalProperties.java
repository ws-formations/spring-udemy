package fr.tuto.spring.springtechnique.confds;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * private String[] db; @Value("{'${dao.db}'.split(',')}")
 * private String[] mysql; @Value("${dao.mysql[0]")
 * /C:/Users/mamadououry.sow/Documents/Formation/J2EE/ws-formations/spring-udemy/configuration/
 *
 * ${CONF_MYSQL}
 * //@PropertySource(value = {"file:${PTOMCAT_CONF_MYSQL}/configuration/database-jdbcconf.properties"})
 * /${TOMCAT_CONF_MYSQL}/configuration/database-jdbcconf.properties
 * @PropertySource(value = {"${project.parent.basedir}/configuration/database-jdbcconf.properties"})
 * @PropertySource(value = {"file:/C:/Users/mamadououry.sow/Documents/Formation/J2EE/ws-formations/spring-udemy/configuration/database-jdbcconf.properties"})
 */


@Component
@ConfigurationProperties(value = "db-conf",ignoreUnknownFields = false )
@PropertySource(value = {"file:${TOMCAT_CONF_MYSQL}/db-conf.properties"})
public class DaoTechnicalProperties {

    @Value("${dao.dbdriver}")
    private  String dbdriver;

    @Value("${dao.urlbase}")
    private  String urlbase;

    @Value("${dao.urlutc}")
    private  String urlutc;

    @Value("${dao.user}")
    private String user;

    @Value("${dao.password}")
    private String password;

    @Value("#{${dao.dbmap}}")
    private Map<String, String> dbmap;

    /**
     * Methode qui retourne l'url de la base mysql
     * @param dbKey = JPA , JDBC, TX
     * @return
     */
    public String getUrlmysql(String dbKey) {
        return urlbase.concat(dbmap.get(dbKey)).concat(urlutc);
    }

    public String getUrlbase() {
        return urlbase;
    }

    public void setUrlbase(String urlbase) {
        this.urlbase = urlbase;
    }

    public String getUrlutc() {
        return urlutc;
    }

    public void setUrlutc(String urlutc) {
        this.urlutc = urlutc;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, String> getDbmap() {
        return dbmap;
    }

    public void setDbmap(Map<String, String> dbmap) {
        this.dbmap = dbmap;
    }

    public String getDbdriver() {
        return dbdriver;
    }

    public void setDbdriver(String dbdriver) {
        this.dbdriver = dbdriver;
    }
}
