-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 21 Septembre 2020 à 06:33
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_udemy_jpa`
--

-- --------------------------------------------------------

--
-- Structure de la table `formations_jpa`
--

CREATE TABLE `formations_jpa` (
  `id` int(11) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `titre` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `formations_jpa`
--

INSERT INTO `formations_jpa` (`id`, `descriptif`, `titre`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Spring Framework: étape par étape pour devenir professionnel'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Hibernate Framework: étape par étape pour devenir professionnel'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Maven Framework: étape par étape pour devenir professionnel');

-- --------------------------------------------------------


--
-- Structure de la table `seq_formation`
--

CREATE TABLE `seq_formation` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `seq_formation`
--

INSERT INTO `seq_formation` (`next_val`) VALUES
(4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `formations_jpa`
--
ALTER TABLE `formations_jpa`
  ADD PRIMARY KEY (`id`);


