SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `db_udemy_jpa`
-- --------------------------------------------------------

CREATE TABLE `formations_jpa` (
  `id` int(11) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `titre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `formations_jpa`
--

INSERT INTO `formations_jpa` (`id`, `descriptif`, `titre`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Spring Framework: étape par étape pour devenir professionnel'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Hibernate Framework: étape par étape pour devenir professionnel'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Maven Framework: étape par étape pour devenir professionnel');

-- --------------------------------------------------------

--
-- Structure de la table `seq_formation`
--

CREATE TABLE `seq_formation` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `seq_formation`
--

INSERT INTO `seq_formation` (`next_val`) VALUES
(4);

ALTER TABLE `formations_jpa`
  ADD PRIMARY KEY (`id`);

