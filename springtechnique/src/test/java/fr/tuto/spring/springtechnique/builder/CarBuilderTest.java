package fr.tuto.spring.springtechnique.builder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CarBuilderTest {

   // @BeforeEach
    void init() {
    }

    //@Test
    void testBuilder3(){

        Car car = new CarBuilder()
                .descriptif("des d")
                .idAndTitre(5L, "titre")
                .build();

        assertNotNull(car);
        assertEquals(car.getTitre(), "titre");
        System.out.println(car);
    }
}
