package fr.tuto.spring.springtechnique.builder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FormationBuilderTest {

    private FormationBuilder formation1;

    private FormationBuilder formation2;

  //  @BeforeEach
    void setUp(){

    }
    //@Test
    void testBuilder1(){
        formation1 = new FormationBuilder.Builder()
                .id(1L)
                .titre("titre")
                .descriptif("desc")
                .build();
        assertNotNull(formation1);
        assertEquals(formation1.getDescriptif(), "desc");
        System.out.println(formation1);
    }

   // @Test
    void testBuilder2(){
        formation2 = new FormationBuilder.Builder()
                .id(1L)
                .idAndTitre(1L, "titre")
                .descriptif("desc")
                .build();
        assertNotNull(formation2);
        assertEquals(formation2.getTitre(), "titre");
        System.out.println(formation2);
    }
}
