package fr.tuto.spring.springservice.jpa;

import fr.tuto.spring.springdomaine.jpa.FormationJpa;

import java.util.List;

public interface FormJpaEMService {

    long countAllFormationJpa();

    FormationJpa createFormationJpa(FormationJpa formation);

    void updateFormationJpa(FormationJpa formation);

    void deleteFormationJpa(FormationJpa id);

    FormationJpa findFormationJpa(Long id);

    List<FormationJpa> findAllFormationJpa();
}
