package fr.tuto.spring.springservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

@Component
public class FormServiceApp implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(FormServiceApp.class);


	public static void main(String[] args) {
		SpringApplication.run(FormServiceApp.class, args);
	}

	@Override
	public void run(String... args){
		log.info("---------- RUN SERVICE -----------");

	}
}

