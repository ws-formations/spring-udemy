package fr.tuto.spring.springservice.jdbc.namedparam;

import fr.tuto.spring.springdomaine.modele.Formation;

import java.util.Collection;
import java.util.List;

public interface FormNamedParamJDBCTemplateService {

    int countFormJdbcTempNamed();

    // ResultSetExtractor
    Formation findAvecLangues(Long id);

    // RowCallbackHandler
    void extractToCsvFormJdbcTempNamed();

    // RowMapper
    List<Formation> findAllFormJdbcTempNamed();

    void createFormJdbcTempNamed(Formation formation);

    void updateFormJdbcTempNamed(Formation formation);

    void deleteFormJdbcTempNamed(Formation formation);

    Formation findFormJdbcTempNamedParId(Long id);

    int countFormJdbcTempNamedParLangue(String langue);

    String findTitreFormJdbcTempNamedParId(Long id);

    void createFormJdbcTempNamed(Collection<Formation> formations);
}
