package fr.tuto.spring.springservice.abstrait;

import fr.tuto.spring.springjdbcdao.factory.DaoJDBCFactory;
import fr.tuto.spring.springjpadao.factory.DaoJPAFactory;
import fr.tuto.spring.springtxdao.factory.DaoTxFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public abstract class AbstractService{

    @Autowired
    private DaoJDBCFactory daoJDBCFactory;

    @Autowired
    private DaoJPAFactory daoJPAFactory;

    @Autowired
    private DaoTxFactory daoTXFactory;


    public DaoJDBCFactory getDaoJDBCFactory(){
        return daoJDBCFactory;
    }

    public DaoJPAFactory getDaoJPAFactory() {
        return daoJPAFactory;
    }

    public DaoTxFactory getDaoTXFactory() {
        return daoTXFactory;
    }
    
}
