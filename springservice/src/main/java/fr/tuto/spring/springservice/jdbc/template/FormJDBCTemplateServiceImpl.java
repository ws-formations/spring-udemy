package fr.tuto.spring.springservice.jdbc.template;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springservice.abstrait.AbstractService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormJDBCTemplateServiceImpl extends AbstractService implements FormJDBCTemplateService {

	@Override
	public int countJDBCTemFormations() {
		return getDaoJDBCFactory().getFormJDBCTemplateDao().countJDBCTemFormations();
	}

	@Override
	public Formation findFormJdbcTempParId(final Long id) {
		return getDaoJDBCFactory().getFormJDBCTemplateDao().findFormJdbcTempParId(id);
	}

	// ResultSetExtractor
	@Override
	public Formation findFormJdbcTemAvecLangues(final Long id) {
		return getDaoJDBCFactory().getFormJDBCTemplateDao().findFormJdbcTempAvecLangues(id);
	}

	// RowCallbackHandler
	@Override
	public void extractJdbcTempFormToCsv() {
		getDaoJDBCFactory().getFormJDBCTemplateDao().extractJdbcTempFormToCsv();
	}

	// RowMapper
	@Override
	public List<Formation> findAllFormJdbcTemp() {
		return getDaoJDBCFactory().getFormJDBCTemplateDao().findAllFormJdbcTemp();
	}

	@Override
	public void createFormJdbcTemp(final Formation form) {
		getDaoJDBCFactory().getFormJDBCTemplateDao().createFormJdbcTemp(form);
	}

	@Override
	public void updateFormJdbcTemp(final Formation formation) {
		getDaoJDBCFactory().getFormJDBCTemplateDao().updateFormJdbcTemp(formation);
	}

	@Override
	public void deleteFormJdbcTemp(final Formation formation) {
		getDaoJDBCFactory().getFormJDBCTemplateDao().deleteFormJdbcTemp(formation);
	}

}
