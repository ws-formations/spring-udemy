package fr.tuto.spring.springservice.jpa;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springservice.abstrait.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormJpaEMServiceImpl extends AbstractService implements FormJpaEMService {

	private static final Logger log = LoggerFactory.getLogger(FormJpaEMServiceImpl.class);

	@Override
	public long countAllFormationJpa() {
		return getDaoJPAFactory().getFormationJpaDao().getCountFormJpa();
	}

	@Override
	public FormationJpa createFormationJpa(FormationJpa formation) {
		return getDaoJPAFactory().getFormationJpaDao().createFormationJpa(formation);
	}

	@Override
	public void updateFormationJpa(FormationJpa formation) {
		getDaoJPAFactory().getFormationJpaDao().updateFormationJpa(formation);
	}


	@Override
	public void deleteFormationJpa(FormationJpa formation) {
		getDaoJPAFactory().getFormationJpaDao().deleteFormationJpa(formation);
	}

	@Override
	public FormationJpa findFormationJpa(Long id) {

		FormationJpa formJpa;
		try {
			formJpa = getDaoJPAFactory().getFormationJpaDao().findFormationJpaParID(id);
		}catch (Exception ex) {
			log.error("service Formation " +ex);
			throw new BusinessException("Impossible de recupérer la liste des formation par le Dao");
		}
		return formJpa;
	}

	@Override
	public List<FormationJpa> findAllFormationJpa() {
		return getDaoJPAFactory().getFormationJpaDao().findAllFormationJpa();
	}
}
