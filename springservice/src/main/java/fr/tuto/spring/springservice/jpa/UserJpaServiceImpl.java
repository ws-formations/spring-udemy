package fr.tuto.spring.springservice.jpa;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springdomaine.jpa.UserJpa;
import fr.tuto.spring.springservice.abstrait.AbstractService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserJpaServiceImpl extends AbstractService implements UserJpaService{


    @Override
    public UserJpa createUserJpa(UserJpa userJpa) {
        return getDaoJPAFactory().getUserJpaDao().createUserJpa(userJpa);
    }

    @Override
    public void updateUserJpa(UserJpa userJpa) {
        getDaoJPAFactory().getUserJpaDao().updateUserJpa(userJpa);
    }

    @Override
    public void deleteUserJpa(UserJpa userJpa) {
        getDaoJPAFactory().getUserJpaDao().deleteUserJpa(userJpa);
    }

    @Override
    public UserJpa findUserJpaParID(Long idUser) {
        try {
            return getDaoJPAFactory().getUserJpaDao().findUserJpaParID(idUser);
        }catch (Exception ex) {
            throw new BusinessException(" FINALLY Service Can't find UserJpa for ID");
        }
    }

    @Override
    public UserJpa findUserJpaByID(Long idUser) {
        return getDaoJPAFactory().getUserJpaDao().findUserJpaByID(idUser);
    }

    @Override
    public List<UserJpa> findAllUsersJpa() {
        return getDaoJPAFactory().getUserJpaDao().findAllUsersJpa();
    }

    @Override
    public long getCountUserJpa() {
        return getDaoJPAFactory().getUserJpaDao().getCountUserJpa();
    }
}
