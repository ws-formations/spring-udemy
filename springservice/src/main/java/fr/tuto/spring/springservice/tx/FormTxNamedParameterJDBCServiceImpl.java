package fr.tuto.spring.springservice.tx;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springservice.abstrait.AbstractService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormTxNamedParameterJDBCServiceImpl extends AbstractService implements FormTxNamedParameterJDBCService{

    @Override
    public int countTxFormations() {
        return getDaoTXFactory().getFormTxNamedParameterJDBCDao().countTxFormations();
    }

    @Override
    public void createTxFormation(Formation form) {
        getDaoTXFactory().getFormTxNamedParameterJDBCDao().createTxFormation(form);
    }

    @Override
    public void createLangueTxForFormation(Formation form, String lang) {
        getDaoTXFactory().getFormTxNamedParameterJDBCDao().createLangueTxForFormation(form,lang);
    }

    @Override
    public void updateTxFormation(Formation form) {
        getDaoTXFactory().getFormTxNamedParameterJDBCDao().updateTxFormation(form);
    }

    @Override
    public void deleteTxFormation(Formation form) {
        getDaoTXFactory().getFormTxNamedParameterJDBCDao().deleteTxFormation(form);
    }

    @Override
    public Formation findTxFormationParId(Long id) {
        return getDaoTXFactory().getFormTxNamedParameterJDBCDao().findTxFormationParId(id);
    }

    @Override
    public List<Formation> findAllTxFormation() {
        return getDaoTXFactory().getFormTxNamedParameterJDBCDao().findAll();
    }

    @Override
    public String findTitreTxFormationParId(Long id) {
        return getDaoTXFactory().getFormTxNamedParameterJDBCDao().findTitreParId(id);
    }
}
