package fr.tuto.spring.springservice.factory;

import fr.tuto.spring.springservice.jdbc.namedparam.FormNamedParamJDBCTemplateService;
import fr.tuto.spring.springservice.jdbc.template.FormJDBCTemplateService;
import fr.tuto.spring.springservice.jpa.FormJpaEMService;
import fr.tuto.spring.springservice.jpa.UserJpaService;
import fr.tuto.spring.springservice.tx.FormTxNamedParameterJDBCService;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public interface ServiceFactory{

    FormJDBCTemplateService getFormationJDBCTemplateService();

    FormNamedParamJDBCTemplateService getFormNamedParamJDBCTemplateService();

    FormJpaEMService getFormJpaEMService();

    UserJpaService getUserJpaService();

    FormTxNamedParameterJDBCService getFormTxNamedParameterJDBCService();
}
