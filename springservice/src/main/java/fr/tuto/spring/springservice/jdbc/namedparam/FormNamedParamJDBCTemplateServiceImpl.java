package fr.tuto.spring.springservice.jdbc.namedparam;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springservice.abstrait.AbstractService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class FormNamedParamJDBCTemplateServiceImpl extends AbstractService implements FormNamedParamJDBCTemplateService {

	@Override
	public int countFormJdbcTempNamed() {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().countFormJdbcTempNamed();
	}

	@Override
	public int countFormJdbcTempNamedParLangue(String langue) {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().countFormJdbcTempNamedParLangue(langue);
	}

	@Override
	public Formation findFormJdbcTempNamedParId(Long id) {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().findFormJdbcTempNamedParId(id);
	}


	@Override
	public String findTitreFormJdbcTempNamedParId(Long id) {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().findTitreFormJdbcTempNamedParId(id);
	}

	// ResultSetExtractor
	@Override
	public Formation findAvecLangues(final Long id) {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().findAvecLangues(id);
	}

	// RowCallbackHandler
	@Override
	public void extractToCsvFormJdbcTempNamed() {
		getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().extractToCsvFormJdbcTempNamed();
	}

	// RowMapper
	@Override
	public List<Formation> findAllFormJdbcTempNamed() {
		return getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().findAllFormJdbcTempNamed();
	}

	@Override
	public void createFormJdbcTempNamed(final Formation formation) {
		getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().createFormJdbcTempNamed(formation);
	}
	@Override
	public void createFormJdbcTempNamed(Collection<Formation> formations) {
		getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().createFormJdbcTempNamed(formations);
	}
	@Override
	public void updateFormJdbcTempNamed(final Formation formation) {
		getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().updateFormJdbcTempNamed(formation);
	}

	@Override
	public void deleteFormJdbcTempNamed(final Formation formation) {
		getDaoJDBCFactory().getFormNamedParamJDBCTemplateDao().deleteFormJdbcTempNamed(formation);
	}

}
