package fr.tuto.spring.springservice.tx;

import fr.tuto.spring.springdomaine.modele.Formation;

import java.util.List;

public interface FormTxNamedParameterJDBCService {

    int countTxFormations();

    void createTxFormation(Formation formation);

    void createLangueTxForFormation(Formation formation, String langue);

    void updateTxFormation(Formation formation);

    void deleteTxFormation(Formation formation);

    Formation findTxFormationParId(Long id);

    List<Formation> findAllTxFormation();

    String findTitreTxFormationParId(final Long id);
}
