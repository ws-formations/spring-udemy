package fr.tuto.spring.springservice.factory;

import fr.tuto.spring.springservice.abstrait.AbstractService;
import fr.tuto.spring.springservice.jdbc.namedparam.FormNamedParamJDBCTemplateService;
import fr.tuto.spring.springservice.jdbc.template.FormJDBCTemplateService;
import fr.tuto.spring.springservice.jpa.FormJpaEMService;
import fr.tuto.spring.springservice.jpa.UserJpaService;
import fr.tuto.spring.springservice.tx.FormTxNamedParameterJDBCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactoryImpl extends AbstractService implements ServiceFactory{

    @Autowired
    private FormJDBCTemplateService formJDBCTemplateService;

    @Autowired
    private FormNamedParamJDBCTemplateService formNamedParamJDBCTemplateService;

    @Autowired
    private FormJpaEMService formJpaEMService;

    @Autowired
    private UserJpaService userJpaService;

    @Autowired
    private FormTxNamedParameterJDBCService formTxNamedParameterJDBCService;

    @Override
    public FormJDBCTemplateService getFormationJDBCTemplateService(){
        return formJDBCTemplateService;
    }

    @Override
    public FormNamedParamJDBCTemplateService getFormNamedParamJDBCTemplateService() {
        return formNamedParamJDBCTemplateService;
    }

    @Override
    public FormJpaEMService getFormJpaEMService() {
        return formJpaEMService;
    }

    @Override
    public UserJpaService getUserJpaService() {
        return userJpaService;
    }

    @Override
    public FormTxNamedParameterJDBCService getFormTxNamedParameterJDBCService() {
        return formTxNamedParameterJDBCService;
    }
}
