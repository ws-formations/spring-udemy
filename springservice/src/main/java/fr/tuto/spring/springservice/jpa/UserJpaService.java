package fr.tuto.spring.springservice.jpa;

import fr.tuto.spring.springdomaine.jpa.UserJpa;

import java.util.List;

public interface UserJpaService {

	UserJpa createUserJpa(UserJpa userJpa);

	void updateUserJpa(UserJpa userJpa);

	void deleteUserJpa(UserJpa formation);

	UserJpa findUserJpaParID(Long artistId);

	UserJpa findUserJpaByID(Long idUser);

	List<UserJpa> findAllUsersJpa();

	long getCountUserJpa();
}
