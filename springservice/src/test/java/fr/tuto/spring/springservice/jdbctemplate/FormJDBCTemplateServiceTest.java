package fr.tuto.spring.springservice.jdbctemplate;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.confjdbc.DaoJdbcConfig;
import fr.tuto.spring.springservice.FormServiceAppTest;
import fr.tuto.spring.springservice.jdbc.template.FormJDBCTemplateService;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DisplayName("Réussir les test Formation JDBC Template Service")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = { FormServiceAppTest.class, DaoJdbcConfig.class})
public class FormJDBCTemplateServiceTest {

	private static final Logger log = LoggerFactory.getLogger(FormJDBCTemplateServiceTest.class);

   @Autowired
	private FormJDBCTemplateService formJdbcTemplateService;


	@Test
	public void test1CreateRunWithoutError() {
		assertThat(formJdbcTemplateService.countJDBCTemFormations()).isEqualTo(4);
		Formation formation = new Formation(5L,"formation 1", "descriptif");
		formJdbcTemplateService.createFormJdbcTemp(formation);
		assertThat(formJdbcTemplateService.findFormJdbcTempParId(5L)).isNotNull();
		assertThat(formJdbcTemplateService.countJDBCTemFormations()).isEqualTo(5);
	}


	@Test
	public void test2FindAllReturn3Items() {
		assertThat(formJdbcTemplateService.findAllFormJdbcTemp()).hasSize(4);

		formJdbcTemplateService.findAllFormJdbcTemp().forEach(formation -> {
			log.info("Ma formation: formation {}", formation);
		});
	}

	@Test
	public void should3FindTitreByIdReturn() {
		assertThat(formJdbcTemplateService.findFormJdbcTempParId(4L).getTitre()).isEqualTo("DAO Maven Hibernate");
	}
}
