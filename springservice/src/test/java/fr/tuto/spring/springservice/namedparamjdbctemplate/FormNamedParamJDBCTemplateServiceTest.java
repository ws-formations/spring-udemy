package fr.tuto.spring.springservice.namedparamjdbctemplate;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springjdbcdao.confjdbc.DaoJdbcConfig;
import fr.tuto.spring.springjpadao.conf.DaoJpaHibernateConfig;
import fr.tuto.spring.springservice.FormServiceAppTest;
import fr.tuto.spring.springservice.jdbc.namedparam.FormNamedParamJDBCTemplateService;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { FormServiceAppTest.class, DaoJdbcConfig.class, DaoJpaHibernateConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class FormNamedParamJDBCTemplateServiceTest {

    @Autowired
    private FormNamedParamJDBCTemplateService beanUnderTest;


    @Test
    public void should1FindAllReturn3Items() {
        assertThat(beanUnderTest.findAllFormJdbcTempNamed()).hasSize(5);
    }

    @Test
    public void should2FindNewlyCreatedFormation() {
        Formation formation = new Formation(5l, "nouvelle formation", "un descriptif");
        beanUnderTest.createFormJdbcTempNamed(formation);
        assertThat(beanUnderTest.findFormJdbcTempNamedParId(5l)).isNotNull();
    }



}
