package fr.tuto.spring.springservice.jpa;

import fr.tuto.spring.springcommon.exception.BusinessException;
import fr.tuto.spring.springdomaine.jpa.FormationJpa;
import fr.tuto.spring.springjpadao.conf.DaoJpaConfig;
import fr.tuto.spring.springservice.FormServiceAppTest;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = { FormServiceAppTest.class, DaoJpaConfig.class})
public class FormJPAServiceTest {

	@Autowired
	private FormJpaEMService formJpaEMService;

	@Test
	public void shouldCreateRunWithoutError() {
		FormationJpa formation = new FormationJpa("formation 1", "descriptif");
		formJpaEMService.createFormationJpa(formation);
		assertThat(formation.getId()).isNotNull();
		assertThat(formJpaEMService.findFormationJpa(formation.getId())).isNotNull();
	}

	@Test
	public void shouldUpdateRunWithoutError() {
		FormationJpa formation = new FormationJpa("formation 1", "descriptif");
		formJpaEMService.createFormationJpa(formation);
		formation.setTitre("titre modifié");
		formJpaEMService.updateFormationJpa(formation);

		assertThat(formJpaEMService.findFormationJpa(formation.getId()).getTitre()).isEqualTo("titre modifié");
	}

	@Test
	public void shouldDeleteRunWithoutError() {
		FormationJpa formation = new FormationJpa("formation 1", "descriptif");
		formJpaEMService.createFormationJpa(formation);

		formJpaEMService.deleteFormationJpa(formation);
		Assertions.assertThrows(BusinessException.class, () -> {
			assertThat(formJpaEMService.findFormationJpa(formation.getId())).isNull();
		});



	}
}
