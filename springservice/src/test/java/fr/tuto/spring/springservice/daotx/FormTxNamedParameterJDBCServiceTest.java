package fr.tuto.spring.springservice.daotx;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springservice.FormServiceAppTest;
import fr.tuto.spring.springservice.tx.FormTxNamedParameterJDBCService;
import fr.tuto.spring.springtxdao.conftx.DaoTxConfig;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = { FormServiceAppTest.class, DaoTxConfig.class })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FormTxNamedParameterJDBCServiceTest {

    @Autowired
    private FormTxNamedParameterJDBCService beanUnderTest;

    @Test
    public void should1FindNewlyCreatedFormation() {
        Formation formation = new Formation(5l, "nouvelle formation", "un descriptif");
        beanUnderTest.createTxFormation(formation);
        assertThat(beanUnderTest.findTxFormationParId(5l)).isNotNull();
    }

    @Test
    public void should2FindTitreByIdReturn() {
        assertThat(beanUnderTest.findTxFormationParId(4L).getTitre()).isEqualTo("JDBC Maven Frameworks");
    }
    @Test
    public void should3FindTitreByIdReturn() {
        assertThat(beanUnderTest.findTitreTxFormationParId(4L)).isEqualTo("JDBC Maven Frameworks");
    }
    @Test
    public void should4FindAllReturn3Items() {
        assertThat(beanUnderTest.findAllTxFormation()).hasSize(4);
    }
}
