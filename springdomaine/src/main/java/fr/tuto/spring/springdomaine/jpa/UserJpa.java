package fr.tuto.spring.springdomaine.jpa;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "USER_JPA")
public class UserJpa extends BaseEntity{

    @Column(nullable = false, name = "PHOTOS")
    private String photo;

    @Column(nullable = false, name = "FIRST_NAME")
    private String firstName;

    @Column(nullable = false, name = "LAST_NAME")
    private String lastName;

    @Column(nullable = false, name = "EMAIL")
    private String email;

    @Column(nullable = false, name = "DRINK_PREFERENCE")
    private String drinkPreference;

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_JPA_HOBBIES", joinColumns = @JoinColumn(name = "USER_JPA_ID"))
    @Column(name = "HOBBIES", nullable = false)
    private List<String> hobbies;

    public UserJpa() {
    }

    public UserJpa(String photo, String firstName, String lastName, String email, String drinkPreference, List<String> hobbies) {
        this.photo = photo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.drinkPreference = drinkPreference;
        this.hobbies = hobbies;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDrinkPreference() {
        return drinkPreference;
    }

    public void setDrinkPreference(String drinkPreference) {
        this.drinkPreference = drinkPreference;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserJpa userJpa = (UserJpa) o;
        return Objects.equals(photo, userJpa.photo) &&
                Objects.equals(firstName, userJpa.firstName) &&
                Objects.equals(lastName, userJpa.lastName) &&
                Objects.equals(email, userJpa.email) &&
                Objects.equals(drinkPreference, userJpa.drinkPreference) &&
                Objects.equals(hobbies, userJpa.hobbies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), photo, firstName, lastName, email, drinkPreference, hobbies);
    }

    @Override
    public String toString() {
        return "UserJpa {" +
                "ID ='" + getId() + '\'' +
                "PHOTOS ='" + photo + '\'' +
                ", FIRST_NAME = '" + firstName + '\'' +
                ", LAST_NAME = '" + lastName + '\'' +
                ", EMAIL = '" + email + '\'' +
                ", DRINK_PREFERENCE ='" + drinkPreference + '\'' +
                ", HOBBIES = " + hobbies +
                '}';
    }
}
