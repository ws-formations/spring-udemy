package fr.tuto.spring.springdomaine.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FORMATION_JPA")
public class FormationJpa extends BaseEntity{

	@Column(nullable = false, name = "TITRE_FORM")
	private String titre;

	@Column(nullable = false, name = "DESC_FORM")
	private String descriptif;

	public FormationJpa(final String titre, final String descriptif) {
		super();
		this.titre = titre;
		this.descriptif = descriptif;
	}

	public FormationJpa() {
		super();
	}


	public String getTitre() {
		return titre;
	}

	public void setTitre(final String titre) {
		this.titre = titre;
	}

	public String getDescriptif() {
		return descriptif;
	}

	public void setDescriptif(final String descriptif) {
		this.descriptif = descriptif;
	}

	@Override
	public String toString() {
		return "Formation [id=" + getId() + ", titre=" + titre + ", descriptif=" + descriptif + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (getId() == null ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FormationJpa other = (FormationJpa) obj;
		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

}
