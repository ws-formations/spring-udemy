package fr.tuto.spring.springdomaine.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Base Entity
 * @author cem ikta
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false, name = "ID", unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_formation")
    @SequenceGenerator(name = "seq_formation", allocationSize = 1)
    private Long id;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    @Override
    public String toString(){
        return "BaseEntity{" + "id=" + id + '}';
    }

    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode(){
        return Objects.hash(id);
    }
}
