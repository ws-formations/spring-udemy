package fr.tuto.spring.springtxdao;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = { "fr.tuto.spring.springtxdao" })
@SpringBootConfiguration
class DaoTxApliTests {


}
