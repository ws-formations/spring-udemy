package fr.tuto.spring.springtxdao.daotx;

import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springtechnique.confds.DaoTXTechnicalProperties;
import fr.tuto.spring.springtxdao.DaoTxAppli;
import fr.tuto.spring.springtxdao.conftx.DaoTxConfig;
import fr.tuto.spring.springtxdao.jdbctx.FormTxNamedParameterJDBCDao;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = { DaoTxAppli.class, DaoTxConfig.class, DaoTXTechnicalProperties.class })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FormTxNamedParameterJDBCDaoTest {

    @Autowired
    private FormTxNamedParameterJDBCDao beanUnderTest;

    @Test
    @Transactional(value = "txPlatformTransactionManager")
    @Rollback
    public void test1_FindNewlyCreatedFormation() {
        Formation formation = new Formation(5l, "nouvelle formation", "un descriptif");
        beanUnderTest.createTxFormation(formation);
        assertThat(beanUnderTest.findTxFormationParId(5l)).isNotNull();
    }

    @BeforeTransaction
    public void beforeTx() {
        assertThat(beanUnderTest.findAll()).hasSize(4);
    }

    @AfterTransaction
    public void afterTx() {
        assertThat(beanUnderTest.findAll()).hasSize(4);
    }

    @Test
    // @Sql(scripts = "classpath:db/nouvelle-formation.sql")
    @SqlGroup({ @Sql(statements = { "INSERT INTO formations_tx VALUES (5, 'nouvelle formation', 'descriptif');" }) })
    public void test2_FindTitreByIdReturn() {
        assertThat(beanUnderTest.findTitreParId(5L)).isEqualTo("nouvelle formation");
    }

    @Test
    public void test3_FindAllReturn3Items() {
        assertThat(beanUnderTest.findAll()).hasSize(4);
    }
}
