package fr.tuto.spring.springtxdao.jdbctx;

import fr.tuto.spring.springcommon.mapper.FormationRowMapper;
import fr.tuto.spring.springdomaine.modele.Formation;
import fr.tuto.spring.springtxdao.abstrait.AbstractJdbcTx;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Repository
public class FormTxNamedParameterJDBCDaoImpl extends AbstractJdbcTx implements FormTxNamedParameterJDBCDao {

	@Override
	public int countTxFormations() {
		return getNamedParameterJdbc().queryForObject("Select count(*) from formations_tx ", new HashMap<>(), Integer.class);
	}

	@Override
	@Transactional(value = "txPlatformTransactionManager")
	public void createTxFormation(final Formation formation) {

		String query = "insert into formations_tx values(:id,:titre, :descriptif)";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("titre", formation.getTitre());
		params.addValue("descriptif", formation.getDescriptif());
		params.addValue("id", formation.getId());
		getNamedParameterJdbc().update(query, params);
	}

	@Override
	@Transactional(value = "txPlatformTransactionManager")
	public void createLangueTxForFormation(final Formation formation, final String langue) {
		String query = "insert into langues values(:id,:langue)";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", formation.getId());
		params.addValue("langue", langue);
		getNamedParameterJdbc().update(query, params);
	}

	@Override
	@Transactional(value = "txPlatformTransactionManager")
	public void updateTxFormation(final Formation formation) {
		String query = "update formations_tx set titre=:titre, descriptif=:descriptif where id=:id";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("titre", formation.getTitre());
		params.addValue("descriptif", formation.getDescriptif());
		params.addValue("id", formation.getId());
		getNamedParameterJdbc().update(query, params);

	}

	@Override
	@Transactional(value = "txPlatformTransactionManager")
	public void deleteTxFormation(final Formation formation) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", formation.getId());
		getNamedParameterJdbc().update(" delete from formations_tx where id=:id", params);
	}

	@Override
	public Formation findTxFormationParId(final Long id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		String query = "select * from formations_tx f where  f.id=:id";
		return getNamedParameterJdbc().query(query, params, new FormationRowMapper()).get(0);
	}

	@Override
	public List<Formation> findAll() {
		return getNamedParameterJdbc().query("select * from formations_tx", new FormationRowMapper());
	}

	@Override
	public String findTitreParId(final Long id) {
		String query = "Select f.titre from formations_tx f where f.id=:id";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		return getNamedParameterJdbc().queryForObject(query, params, String.class);
	}
}
