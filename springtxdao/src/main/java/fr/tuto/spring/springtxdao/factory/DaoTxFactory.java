package fr.tuto.spring.springtxdao.factory;

import fr.tuto.spring.springtxdao.jdbctx.FormTxNamedParameterJDBCDao;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public interface DaoTxFactory {

     FormTxNamedParameterJDBCDao getFormTxNamedParameterJDBCDao();
}
