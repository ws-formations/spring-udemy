package fr.tuto.spring.springtxdao.factory;

import fr.tuto.spring.springtxdao.jdbctx.FormTxNamedParameterJDBCDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DaoTxFactoryImpl implements DaoTxFactory {

    @Autowired
    private FormTxNamedParameterJDBCDao formTxNamedParameterJDBCDao;

    @Override
    public FormTxNamedParameterJDBCDao getFormTxNamedParameterJDBCDao(){
        return formTxNamedParameterJDBCDao;
    }
}
