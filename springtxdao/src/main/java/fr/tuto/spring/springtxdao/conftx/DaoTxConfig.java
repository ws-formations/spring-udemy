package fr.tuto.spring.springtxdao.conftx;

import fr.tuto.spring.springtechnique.confds.DaoTXTechnicalProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan("fr.tuto.spring.springtxdao")
@EnableTransactionManagement
public class DaoTxConfig {

	private static final Logger log = LoggerFactory.getLogger(DaoTxConfig.class);

	@Autowired
	private DaoTXTechnicalProperties daoTXTechnicalProperties;

	@Profile("!test")
	@Bean(name = "txDataSource")
	public DriverManagerDataSource txDataSource() {
		log.info("URRL Module TX DAO = "+ daoTXTechnicalProperties.getUrlmysql("TX"));
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(daoTXTechnicalProperties.getDbdriver());
		dataSource.setUrl(daoTXTechnicalProperties.getUrlmysql("TX"));
		dataSource.setUsername(daoTXTechnicalProperties.getUser());
		dataSource.setPassword(daoTXTechnicalProperties.getPassword());
		return dataSource;
	}

	 @Profile("test")
	 @Bean(name = "txDataSource")
	 public DataSource txDataSourceH2() {
	 	return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).addScript("classpath:dbtx/schema_tx.sql")
	 			.addScript("classpath:dbtx/data_tx.sql").build();
	 }

	@Profile("!test")
	@Bean(name = "txPlatformTransactionManager")
	PlatformTransactionManager txPlatformTransactionManager(@Qualifier("txDataSource") final DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Profile("test")
	@Bean(name = "txPlatformTransactionManager")
	PlatformTransactionManager txTransactionManagerH2(@Qualifier("txDataSource") final DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Profile("!test")
	@Bean(name = "txNamedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate txNamedParameterJdbcTemplate(@Qualifier("txDataSource") final DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Profile("test")
	@Bean(name = "txNamedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate txNamedParameterJdbcTemplateH2(@Qualifier("txDataSource") final DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean(name = "txTransactionTemplate")
	public TransactionTemplate txTransactionTemplate(@Qualifier("txPlatformTransactionManager") final PlatformTransactionManager transactionManager) {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		transactionTemplate.setTimeout(-1);
		transactionTemplate.setReadOnly(false);
		return transactionTemplate;
	}

}
