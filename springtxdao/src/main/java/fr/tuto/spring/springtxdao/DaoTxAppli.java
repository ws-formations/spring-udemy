package fr.tuto.spring.springtxdao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DaoTxAppli implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(DaoTxAppli.class);

	@Override
	public void run(String... args) {
		log.info("---------- RUN TX DAO ------------");
	}

}
