package fr.tuto.spring.springtxdao.jdbctx;

import fr.tuto.spring.springdomaine.modele.Formation;

import java.util.List;

/**
 Created By Mamadou Oury SOW On 20/09/2020 **/
public interface FormTxNamedParameterJDBCDao {
    int countTxFormations();

    void createTxFormation(Formation formation);

    void createLangueTxForFormation(Formation formation, String langue);

    void updateTxFormation(Formation formation);

    void deleteTxFormation(Formation formation);

    Formation findTxFormationParId(Long id);

    List<Formation> findAll();

    String findTitreParId(final Long id);
}
