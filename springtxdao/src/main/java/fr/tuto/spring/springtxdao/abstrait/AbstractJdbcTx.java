package fr.tuto.spring.springtxdao.abstrait;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 Created By Mamadou Oury SOW On 19/09/2020 **/
public abstract class AbstractJdbcTx {

    @Autowired
    @Qualifier("txNamedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbc;


    public NamedParameterJdbcTemplate getNamedParameterJdbc(){
        return namedParameterJdbc;
    }
}
